<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 25.10.2015
 * Time: 22:12
 */

namespace Core;

use Core\QB\DB;
use Plugins\Profiler\Profiler;
use Admin\Module\Base as Abase;
/**
 *  Class for routing on the site
 */
class Route {

    static $_instance; // Singletone static variable

    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }

    protected $_params; // Parameters for current page

    protected $_module; // Current module
    protected $_controller; // Current controller
    protected $_action; // Current action

    protected $_admin;
    public $_defaultAction = 'index'; // Default action
    public $_uri; // Current URI
    public $_modules = array(); // Modules we include to project

    protected $_routes = array(); // Array with routes on the full site
    protected $_regular = 'a-zA-Z0-9-_\\\[\]\{\}\:\,\*'; // List of good signs in regular expressions in routes

    function __construct() {
        $this->_admin = false;
        $this->setModules();
        $this->setRoutes();
        $url = $this->get_Url();
        //$this->run();
        //$this->setLanguage();
    }
    //получим все модули зарегистрированные в системе
    protected function setModules() {
        $modul = $this->get_Module();
        foreach($modul as $obj){
            $this->_modules[] = $obj->name;
        }
    }
    //читаем модули из базы
    public function get_Module(){
        $result = DB::select()
            ->from('module')
            //->where('active', '=', 1)
            ->as_object()
            ->execute();
        return $result;
    }
    //читаем у всех модулей файл Route, если есть
    public function setRoutes() {
        // Routes from modules
        foreach ($this->_modules as $module) {
            $path = HOST.APPLICATION.'/Module/'.$module.'/Route.php';
            if(file_exists($path)) {
                $config = require_once $path;
                if (is_array($config) && !empty($config)) {
                    $this->_routes += $config;
                }
            }
        }
        // Default route
        $this->_routes['<module>/<controller>/<action>'] = '<module>/<controller>/<action>';
    }
    //Основная функция получения коректного урла
    public function getUrl(){
        $urls = explode('?',$_SERVER["REQUEST_URI"]);//отсеем переменные
        if(isset($urls[1]))
            $this->_params['?'] = $urls[1];
        $url = explode('/',$urls[0]);//получим чистый урл
        if(isset($url[0]) && $url[0]=='') unset($url[0]);//удалим пустоту если есть
        if(isset($url[1]) && $url[1]=='admin') {
            unset($url[1]);//удалим админ если есть
            $this->_admin = true;//пометим что мы в админке
        }
        //в цикле одновим все
        foreach($url as $obj){
            $links[] = $obj;
        }
        $this->_params['links'] = $links;
        //теперь создадим правельный урл и отсеем не нужное
        for($i = 0; $i<3;$i++){
            if(isset($links[$i]))
                $link[$i] = $links[$i];
        }
        return $link;
    }
    //получим все возможные переменные
    public function get_post(){
        $param = '';
        $link = $this->_params['links'];
        if(isset($this->_params['?']))
            $param = $this->_params['?'];
        if(isset($link[3])){//проверим нет ли переменных
            if (is_numeric($link[3])){//проверим не число ли
                $this->_params["id"] = (int)$link[3];//значит id пишем в post
                $_POST['id']= (int)$link[3];
            }
        }
        if($param != ''){
            $param = explode('&',$param);//отсеем переменные
            foreach($param as $obj){
                $tmp = explode('=',$obj);//отсеем переменные
                $_POST[$tmp[0]]=$tmp[1];

            }
        }
    }
    //почистим урл от лишнего и определим переменные
    public function get_Url(){
        $link = $this->getUrl();//получим чистый урл
        $this->get_post();//получим переменные в post
        //теперь можно определить что где
        $this->_module = $link[0]=='' ? 'Index': $link[0];
        $this->_controller = $link[1]=='' ? 'Index': $link[1];
        $this->_action  = $link[2]=='' ? 'Index': $link[2];
        //поищем на всякий в массиве роута, и перебьем если нашли

        $rot_link = implode('/',$link);
        $this->find_Route($rot_link);
    }
    //поиск совпадения по массиву из роута
    public function find_Route($url){
        foreach($this->_routes as $pattern => $route) {
             if( $pattern == $url ) {//нашли и теперь можно переопределить что где
                $routes = explode('/',$route);//разобьем на параметры и переопределим
                $this->_module = $routes[0]=='' ? 'Index': $routes[0];
                $this->_controller = $routes[1]=='' ? 'Index': $routes[1];
                $this->_action  = $routes[2]=='' ? 'Index': $routes[2];
                 return;
             }
        }

    }
    /**
     *  Run controller->action
     */
    public function start($path, $action) {
        //var_dump($path);die;
        //var_dump($action);
        require_once HOST.APPLICATION.'/Module/Base.php';
        //if(count(APPLICATION)>1){
        Abase::factory()->before();
        //}
        $action .= 'Action';
        $controller = implode('\\', $path);

        //var_dump($action);
        $controller = new $controller;
        $controller->before();
        $token = Profiler::start('Profiler', 'Center');
        $controller->{$action}();
        //Profiler::stop($token);
        $controller->after();
        return true;
    }
    /**
     *  Get all parameters on current page
     */
    public static function params() {
        return Route::factory()->getParams();
    }
    public static function param( $key ) {
        return Route::factory()->getParam( $key );
    }
    /**
     *  Get current controller
     */
    public static function controller() {
        $ret =Route::factory()->getController();
        if($ret == NULL){
            $ret = Route::factory()->setController('Index');
        }
        return $ret;
        //return Route::factory()->getController();
    }
    /**
     *  Get current action
     */
    public static function action() {
        $ret = Route::factory()->getAction();
        if($ret == NULL){
            $ret = Route::factory()->setAction('Index');
        }
        return $ret;
    }
    /**
     *  Get current module
     */
    public static function module() {
        $ret =Route::factory()->getModule();
        if($ret == NULL){
            $ret = Route::factory()->setModule('Index');
        }
        return $ret;
    }
    /**
     *  Real function to get all parameters on current page
     */
    public function getParams() {
        return $this->_params;
    }
    /**
     *  Set parameter to parameters array
     *  @param string $key   - alias for parameter we set
     *  @param string $value - value for parameter we set
     */
    public function setParam( $key, $value ) {
        $this->_params[$key] = $value;
    }
    /**
     *  Real function to get one parameter by alias
     *  @param string $key - alias for parameter we need
     *  @return mixed      - parameter $key from $_params if exists or NULL if doesn't exist
     */
    public function getParam( $key ) {
        return Arr::get($this->_params, $key, NULL);
    }
    /**
     *  Real function to get controller
     */
    public function getController() {
        return $this->_controller;
    }
    /**
     *  Real function to get action
     */
    public function getAction() {
        return $this->_action;
    }
    /**
     *  Real function to get module
     */
    public function getModule() {
        return $this->_module;
    }
    /**
     *  Set action
     */
    public function setAction($fakeAction) {
        return $this->_action = $fakeAction;
    }
    public function setModule($fakeModule) {
        return $this->_module = $fakeModule;
    }
    public function setController($fakeController) {
        return $this->_controller = $fakeController;
    }
    /**
     *  Set route parameters
     *  @param string $route  - current route
     *  @param array  $params - parameters for current route
     *  @return                 boolean
     */
    protected function set( $route, $params = array() ) {
        $array = explode('/', $route);
        // Set module
        if (isset($params['module'])) {
            $this->_module = Arr::get($params, 'module', NULL);
            unset($params['module']);
        } else {
            $this->_module = Arr::get($array, 0, NULL);
        }
        // Set controller
        if (isset($params['controller'])) {
            $this->_controller = Arr::get($params, 'controller', NULL);
            unset($params['controller']);
        } else {
            $this->_controller = Arr::get($array, 1, NULL);
        }
        // Set action
        if (isset($params['action'])) {
            $this->_action = Arr::get($params, 'action', NULL);
            unset($params['action']);
        } else {
            $this->_action = Arr::get($array, 2, $this->_defaultAction);
        }
        // Set else parameters
        foreach ($params as $key => $value) {
            $this->setParam($key, $value);
        }
        return true;
    }
    //проверим в админку мы зашли или на фронт
    public function get_Admins(){
        if(strlen($_SERVER["REQUEST_URI"])>1){
            if(strpos($_SERVER["REQUEST_URI"], 'admin')>0){
                return true;
            }
        }
        return false;
    }
    /**
     *  Set current URI
     */
    protected function setURI() {
        if(!empty($_SERVER['REQUEST_URI'])) {
            $tmp = rtrim(Arr::get($_SERVER, 'REQUEST_URI'), '/');
            if($tmp[0] == '/') {
                $tmp = substr($tmp, 1, strlen($tmp) - 1);
            }
            $tmp = explode('?', $tmp);
            $this->setGET(Arr::get($tmp, 1));
            return $this->_uri = $tmp[0];
        }
        if(!empty($_SERVER['PATH_INFO'])) {
            $tmp = rtrim(Arr::get($_SERVER, 'PATH_INFO'), '/');
            if($tmp[0] == '/') {
                $tmp = substr($tmp, 1, strlen($tmp) - 1);
            }
            $tmp = explode('?', $tmp);
            $this->setGET(Arr::get($tmp, 1));
            return $this->_uri = $tmp[0];
        }
        if(!empty($_SERVER['QUERY_STRING'])) {
            $tmp = rtrim(Arr::get($_SERVER, 'QUERY_STRING'), '/');
            if($tmp[0] == '/') {
                $tmp = substr($tmp, 1, strlen($tmp) - 1);
            }
            $tmp = explode('?', $tmp);
            $this->setGET(Arr::get($tmp, 1));
            return $this->_uri = $tmp[0];
        }
    }
    /**
     *  Set GET parameters
     *  @param string $get - all after "?" in current URI
     */
    protected function setGET( $get ) {
        $get = explode('&', $get);
        foreach ($get as $element) {
            $g = explode('=', $element);
            $_GET[Arr::get($g, 0)] = Arr::get($g, 1);
        }
    }

    public function error() {
        require_once HOST.'/Module/Base.php';
        Config::error();
        $controller = new \Module\Base();
        $controller->before();
        $controller->after();
        return false;
    }

    public function count_Module(){
        $result = DB::select(array(DB::expr('COUNT(id)'), 'count'))
            ->from('module');
        //->where('active', '=', 1);
        return $result->count_all();
    }

}