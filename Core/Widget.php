<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 15.11.2015
 * Time: 14:33
 */

namespace Core;

use Plugins\Profiler\Profiler;
use Core\QB\DB;
use Core\Route;

class Widget
{
    static $_instance; // Constant that consists self class

    public $_data = array(); // Array of called widgets
    public $_tree = array(); // Only for catalog menus on footer and header. Minus one query
    public $_module = array();
    public $_config = array();
    // Instance method
    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }

    /**
     *  Get widget
     *  @param  string $name  [Name of template file]
     *  @param  array  $array [Array with data -> go to template]
     *  @return string        [Widget HTML]
     */
    public static function get( $name, $array = array(), $save = true, $cache = false ) {
        $arr = explode('_', $name);
        $viewpath = implode('/', $arr);

            $token = Profiler::start('Profiler', 'Widget '.$name);
        /*if( APPLICATION && !Config::get('error') ) {
            $w = WidgetsBackend::factory();
        } else {
            $w = Widget::factory();
        }*/
        $w = Widget::factory();
        $_cache = Cache::instance();
        if($cache) {
            if (!$_cache->get($name)) {
                $data = NULL;
                if ($save && isset($w->_data[$name])) {
                    $data = $w->_data[$name];
                } else {
                    if( $save && isset( $w->_data[ $name ] ) ) {
                        $data = $w->_data[ $name ];
                    } else if( method_exists( $w, $name ) ) {
                        $result = $w->$name($array);
                        if( $result !== NULL && $result !== FALSE ) {
                            $array = array_merge($array, $result);
                            $data = View::widget( $array, $viewpath);
                        } else {
                            $data = NULL;
                        }
                    } else {
                        $data = $w->common( $viewpath, $array );
                    }
                }
                $_cache->set($name, HTML::compress($data, true));
                return $w->_data[$name] = $data;
            } else {
                return $_cache->get($name);
            }
        }
        if($_cache->get($name)) {
            $_cache->delete($name);
        }
        if( $save && isset( $w->_data[ $name ] ) ) {
            return $w->_data[ $name ];
        }
        if( method_exists( $w, $name ) ) {
            $result = $w->$name($array);
            if( $result !== NULL && $result !== FALSE ) {
                if(is_array($result)) {
                    $array = array_merge($array, $result);
                }
                return $w->_data[$name] = View::widget( $array, $viewpath);
            } else {
                return $w->_data[$name] = NULL;
            }
        }
        return $w->_data[$name] = $w->common( $viewpath, $array );
    }

    /**
     *  Common widget method. Uses when we have no widgets called $name
     *  @param  string $viewpath  [Name of template file]
     *  @param  array  $array     [Array with data -> go to template]
     *  @return string            [Widget HTML or NULL if template doesn't exist]
     */
    public function common( $viewpath, $array ) {
        if( file_exists(HOST.'/Views/Widgets/'.$viewpath.'.php') ) {
            return View::widget($array, $viewpath);
        }
        return NULL;
    }
    public function HiddenData() {
        $cart = Cart::factory()->get_list_for_basket();
        return array( 'cart' => $cart );
    }

    public function Item_Comments() {
        $id = Route::param('id');
        if( !$id ) { return $this->_data['comments'] = ''; }
        $result = DB::select()->from('catalog_comments')->where('status', '=', 1)->where('catalog_id', '=', $id)->order_by('date', 'DESC')->find_all();
        return array( 'result' => $result );
    }
    public function getMenuAdmin(){
        $link_r = Route::factory()->getUrl();
        $link = $link_r[0].'/'.$link_r[1];
        if($link == '/')
            $link = "index/index";

        $result = DB::select()
            ->from('menu')
            ->where('status', '=', 1)
            ->where('id_parent', '=', 0)
            //->order_by('id_parent', 'ASC')
            ->order_by('sort', 'ASC')
            ->find_all();

        foreach ($result as $obj) {
            $tm = $obj;
            if($obj->link == $link){
                $tm->check = true;
                $this->_module['icon'] = $obj->icon;
                $this->_module['autor'] = $obj->author;
                $this->_module['data'] = $obj->created_at;
            }else{$tm->check = false;}
            $tm->subMenu = Widget::getItemSub($obj->id,$link);
            $menu[] = $tm;
        }
        return $menu;
    }
    public function getItemSub($parent,$link){
        $result = DB::select()
            ->from('menu')
            ->where('status', '=', 1)
            ->where('id_parent', '=', $parent)
            ->order_by('sort', 'ASC')
            ->find_all();
        foreach($result as $obj){
            $tm = $obj;
            if($obj->link == $link){
                $tm->check = true;
                $this->_module['icon'] = $obj->icon;
                $this->_module['autor'] = $obj->author;
                $this->_module['data'] = $obj->created_at;
            }else{$tm->check = false;}
            $sub[] =$tm;
        }
        return $sub;
    }
    public function getData($data,$format = 'd.m.Y H:i'){
        return date( $format,$data);
    }

}