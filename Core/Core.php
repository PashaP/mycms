<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 27.10.2015
 * Time: 7:49
 */
namespace Core;

use Core\Arr;
use Core\QB\DB;

Class Core {
    static $_instance;

    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }

    protected $_params; // Parameters for current page
    protected $_controller; // Current controller
    protected $_action; // Current action
    protected $_module; // Current module


    function Init()
    {
        date_default_timezone_set(Config::get('default.timeZone'));
        setlocale(LC_ALL, Config::get('default.charset'));

        if( !file_exists(HOST.APPLICATION.'/Module/Base.php') ) {
            return Config::error();
        }

        Route::factory();
        $this->access();
        $module = ucfirst(Route::module());
        $controller = ucfirst(Route::controller());
        $action = Route::action();

        //var_dump($action);

       /* $email = Arr::get( $_POST, 'email' );
        $password = Arr::get( $_POST, 'password' );
        $remember = Arr::get( $_POST, 'remember' );

        echo $email;
        echo $password;
        echo $remember;*/
        /*echo $module.'</br>';
        echo $controller.'</br>';
        echo $action.'</br>';*/

//die('123');
        if(Route::factory()->get_Admins()){$path[] = 'Admin';}
        $path[] = 'Module';
        if( $module ) { $path[] = $module; }
        $path[] = 'Controllers';
        $path[] = $controller;

        //var_dump(HOST.'/'.implode('/', $path).'.php');die;
        if( file_exists(HOST.'/'.implode('/', $path).'.php') ) {
            return Route::factory()->start ($path, $action);
        }

        return Route::factory()->error();


    }
    //проверим имеет ли сюда доступ юзер и прошол ли авторизацию
    private function access(){
        if(Route::factory()->get_Admins()){
            if(User::factory()->User_Status() == false){
                if($_SERVER["REQUEST_URI"]=='/admin/user/auth'){
                    Route::factory()->setModule('user');
                    Route::factory()->setController('auth');
                    Route::factory()->setAction('login');
                    return;
                }
                if($_SERVER["REQUEST_URI"]=='/admin/ajax/login'){
                        Route::factory()->setModule('ajax');
                        Route::factory()->setController('ajax');
                        Route::factory()->setAction('login');
                        return;
                }
                header ('Location: '.'/admin/user/auth');  // перенаправление на нужную страницу
                exit();    // прерываем работу скрипта, чтобы забыл о прошлом
            }else{//мы авторизованы
                if($_SERVER["REQUEST_URI"]=='/admin/user/auth'){
                    header ('Location: '.'/admin');  // перенаправление на нужную страницу
                    exit();    // прерываем работу скрипта, чтобы забыл о прошлом
                }
                /*var_dump($_SERVER["REQUEST_URI"]);
                die('222');*/
            }
        }
    }

    function GetMessage ($type, $message, $time = 3500) {
        $message = addslashes($message);
        switch($type){
            case 1:
                $type='success';
                break;
            case 2:
                $type='danger';
                break;
            case 3:
                $type='info';
                break;
            default:
                $type='warning';
        }

        $_SESSION['GLOBAL_MESSAGE'] = '<script type="text/javascript">$(document).ready(function(){generate("'.$message.'", "'.$type.'", '.(int) $time.');});</script>';
    }
}