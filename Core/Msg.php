<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 15.11.2015
 * Time: 8:47
 */

namespace Core;


class Msg
{
    static $_instance;
    public $Msgs;

    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }
    //создаем сообщение
    public function setMsg($type,$icon,$title,$text,$time,$param){
        $this->Msgs = array('type' => $type, 'ico' => $icon, 'title' => $title, 'text' => $text, 'time' => $time, 'param' => $param);
    }
    //чистим
    public function resetMsg(){
        $this->Msgs = array();
    }
    //возвращаем верстку сообщения
    public function setMessages($type,$title,$text){
        switch ($type) {
            case 'err':
                $ico = 'fa-exclamation-triangle';
                break;
            case 'ok':
                $ico = 'fa-check-square-o';
                break;
            case 'comment':
                $ico = 'fa-comment-o';
                break;
            case 'info':
                $ico = 'fa-info-circle';
                break;

            default:
                $ico = 'fa-check-square-o';
        }

        return View::tpl( array( 'ico' => $ico, 'title' => $title, 'text' => $text, 'class' => $type),'Widgets/HTML/Msg');
    }
}