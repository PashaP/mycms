<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 25.10.2015
 * Time: 21:13
 */

ini_set('display_errors', 'on'); // Display all errors on screen
// Добавлять сообщения обо всех ошибках, кроме E_NOTICE E_STRICT E_DEPRECATED
# error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);

ob_start();//включает буферизацию вывода.
@session_start();//инициализирует данные сессии.

header("Cache-Control: public");
header("Expires: " . date("r", time() + 3600));
header('Content-Type: text/html; charset=UTF-8');

define('HOST', dirname(__FILE__)); // Root path
define('APPLICATION', ''); // Choose application - View or frontend. If frontend - set ""
define('PROFILER', FALSE); // On/off profiler
define('START_TIME', microtime(TRUE)); // For profiler. Don't touch!
define('START_MEMORY', memory_get_usage()); // Возвращает объем памяти, выделяемой для PHP

require_once 'autoloader.php'; // Autoloader

Core\Core::factory()->Init();
//Plugins\Profiler\Profiler::view();
