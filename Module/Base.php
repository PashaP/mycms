<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 27.10.2015
 * Time: 19:51
 */

namespace Module;

use Core\Config;
//use Core\HTTP;
use Core\HTML;
use Core\QB\DB;
use Core\User;
use Core\View;

class Base
{
    protected $_template = 'Articles';
    protected $_content;
    protected $_config = array();
    protected $_seo = array();
    protected $_breadcrumbs = array();
    protected $_method;

    public function before() {
        $this->_method = $_SERVER['REQUEST_METHOD'];
        $this->config();
        //$this->access();
        //$this->redirects();
        //User::factory()->is_remember();
        //$cron = new Cron;
       // $cron->check();
    }
    public function after() {
        //$this->seo();
        //$this->visitors();

        $this->render();
    }
    private function config() {
       /* $result = DB::select('key', 'zna', 'group')
            ->from('config')
            ->join('config_groups')->on('config.group', '=', 'config_groups.alias')
            ->where('config.status', '=', 1)
            ->where('config_groups.status', '=', 1)
            ->find_all();
        $groups = array();
        foreach($result AS $obj) {
            $groups[$obj->group][$obj->key] = $obj->zna;
        }
        foreach($groups AS $key => $value) {
            Config::set($key, $value);
        }*/
        //$result = DB::select('script', 'place')->from('seo_scripts')->where('status', '=', 1)->as_object()->execute();
        //$this->_seo['scripts'] = array('body' => array(), 'head' => array(), 'counter' => array());
        //foreach ( $result AS $obj ) {
        //    $this->_seo['scripts'][ $obj->place ][] = $obj->script;
        //}

        $this->setBreadcrumbs('Главная', '');
    }

    private function render() {
        if( Config::get( 'error' ) ) {
            $this->_template = '404';
        }

        $this->_breadcrumbs = HTML::breadcrumbs($this->_breadcrumbs);
        $data = array();
        foreach ($this as $key => $value) {
            $data[$key] = $value;
        }
        //$data['GLOBAL_MESSAGE'] = System::global_massage();

        echo HTML::compress(View::tpl($data, $this->_template));
    }

    public function getAplicate(){

        return "View";
    }


    protected function setBreadcrumbs( $name, $link = NULL ) {
        $this->_breadcrumbs[] = array('name' => $name, 'link' => $link);
    }

}