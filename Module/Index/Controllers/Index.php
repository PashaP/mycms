<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 03.11.2015
 * Time: 23:11
 */

namespace Module\Index\Controllers;

use Core\Arr;
use Core\Core;
use Core\View;

class Index extends \Module\Base
{
    public function before()
    {
        $this->_template = 'Main';

    }

    public function indexAction()
    {
        $this->html = Arr::init_object();
        $this->html->title = 'Главная';
        $this->_content = View::tpl( array( 'result' => $result, 'pager' => $pager,'html' => $this->html, ), $this->_template );
    }
}