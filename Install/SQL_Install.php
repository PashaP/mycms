<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 14.11.2015
 * Time: 13:09
 */
CREATE TABLE myCms.user
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    login VARCHAR(128) NOT NULL,
    view INT NOT NULL,
    ico VARCHAR(128) NOT NULL,
    description TEXT NOT NULL,
    create_at INT NOT NULL,
    modif_at INT NOT NULL
);

CREATE TABLE myCms.module
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    view INT NOT NULL,
    ico VARCHAR(128) NOT NULL,
    description TEXT NOT NULL,
    create_at INT NOT NULL,
    modif_at INT NOT NULL
);
INSERT INTO `myCms`.`module` (`id`, `name`, `view`, `ico`, `description`, `create_at`, `modif_at`)
VALUES (NULL, 'Index', '1', '', '', TIMESTAMP(''), ''), (NULL, 'Ajax', '1', '', '', UNIX_TIMESTAMP(), '');