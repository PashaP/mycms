-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 05 2015 г., 08:58
-- Версия сервера: 5.5.41-log
-- Версия PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `myCms`
--

-- --------------------------------------------------------

--
-- Структура таблицы `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_name` varchar(128) NOT NULL,
  `key_val` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `config`
--

INSERT INTO `config` (`id`, `key_name`, `key_val`) VALUES
(1, 'user_key', 'secure_key');

-- --------------------------------------------------------

--
-- Структура таблицы `configure`
--

CREATE TABLE IF NOT EXISTS `configure` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL COMMENT 'алиас переменной по которой будет поиск и сохранение',
  `title` varchar(255) NOT NULL COMMENT 'надпись выводимая для переменной',
  `value` text NOT NULL COMMENT 'значение переменной',
  `set_val` text NOT NULL COMMENT 'возможные варианты переменной',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) NOT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'будет ли проверка при сохранении',
  `type` varchar(32) DEFAULT NULL COMMENT 'тип переменной',
  `group` varchar(128) DEFAULT NULL COMMENT 'тип группы переменной',
  `create_at` int(10) NOT NULL COMMENT 'дата создания',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `type` (`type`),
  KEY `group` (`group`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='основные настройки сайта' AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `configure`
--

INSERT INTO `configure` (`id`, `name`, `title`, `value`, `set_val`, `status`, `sort`, `valid`, `type`, `group`, `create_at`) VALUES
(1, 'name_site', 'Название сайта', '5 Okean', '', 1, 1, 1, 'input', 'basic', 2015),
(2, 'url_site', 'Наименование "Владельца сайта" для Title на каждой странице', '5 Okean', '', 1, 0, 1, 'input', 'basic', 2015),
(3, 'admin_email', 'E-Mail администратора сайта (отправитель по умолчанию)', 'pir_pilat_pasha', '', 1, 0, 1, 'input', 'mail', 2015),
(4, 'smtp', 'Использовать СМТП', '0', '[{"key":"Да","value":1},{"key":"Нет","value":0}]', 1, 0, 1, 'radio', 'mail', 1448197063),
(5, 'host', 'SMTP server', '111111', '', 1, 0, 1, 'input', 'mail', 1448197063),
(6, 'username', 'Логин', 'admin', '', 1, 0, 1, 'input', 'mail', 0),
(7, 'password', 'Пароль', 'qwerty', '', 1, 0, 1, 'password', 'mail', 0),
(8, 'secure', 'Тип подключения', 'tls', '[{"key":"TLS","value":"tls"},{"key":"SSL","value":"ssl"}]', 1, 0, 1, 'select', 'mail', 0),
(9, 'port', 'Порт. Например 465 или 587. (587 по умолчанию)', '587', '', 1, 0, 1, 'input', 'mail', 0),
(10, 'auth', 'Запаролить сайт', '0', '[{"key":"Да","value":"1"},{"key":"Нет","value":"0"}]', 1, 0, 1, 'radio', 'security', 0),
(11, 'vk', 'VK.com', 'no', '', 1, 0, 1, 'input', 'socials', 0),
(12, 'minify', 'Сократить CSS u JavaScript', '1', '[{"key":"Да","value":"1"},{"key":"Нет","value":"0"}]', 1, 0, 1, 'radio', 'speed', 1448197708),
(13, 'compress', 'Сократить HTML', '0', '[{"key":"Да","value":"1"},{"key":"Нет","value":"0"}]', 1, 0, 1, 'radio', 'speed', 0),
(14, 'cache_images', 'Кеширование размера изображений', '1', '[{"key":"Выключить","value":"0"},{"key":"12 часов","value":"0.5"},{"key":"День","value":"1"},{"key":"3 дня","value":"3"},{"key":"Неделя","value":"7"},{"key":"2 недели","value":"14"},{"key":"Месяц","value":"30"},{"key":"Год","value":"365"}]', 1, 0, 1, 'select', 'speed', 1448197861),
(15, 'copyright', 'Copyright', '5 Okean Copyright', '', 1, 0, 1, 'input', 'static', 1448197861);

-- --------------------------------------------------------

--
-- Структура таблицы `configure_groups`
--

CREATE TABLE IF NOT EXISTS `configure_groups` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `icon` varchar(64) NOT NULL DEFAULT 'fa-folder-o',
  `side` varchar(16) NOT NULL COMMENT 'позиция вывода возможно (right, left)',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`) COMMENT 'alias должен быть уникальным'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='в какой группе будет вывод переменной' AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `configure_groups`
--

INSERT INTO `configure_groups` (`id`, `name`, `alias`, `icon`, `side`, `status`, `sort`) VALUES
(1, 'Базовые', 'basic', 'fa-pencil-square-o', 'left', 1, 0),
(2, 'Безопасность', 'security', 'fa-user-secret', 'left', 1, 1),
(3, 'Почта', 'mail', 'fa-envelope-o', 'left', 1, 2),
(4, 'Соц. сети', 'socials', 'fa-weixin', 'left', 1, 3),
(10, 'Статика', 'static', 'fa-bolt', 'right', 1, 0),
(11, 'Быстродействие', 'speed', 'fa-rocket', 'right', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `configure_types`
--

CREATE TABLE IF NOT EXISTS `configure_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `alias` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`) COMMENT 'alias должен быть уникальным'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='тип полей для форм' AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `configure_types`
--

INSERT INTO `configure_types` (`id`, `name`, `alias`) VALUES
(1, 'Однострочное текстовое поле', 'input'),
(2, 'Текстовое поле', 'textarea'),
(3, 'Выбор значения из списка', 'select'),
(4, 'Пароль', 'password'),
(5, 'Радиокнопка', 'radio'),
(6, 'Текстовое поле с редактором', 'tiny');

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_parent` int(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `icon` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` int(10) NOT NULL,
  `updated_at` int(10) NOT NULL,
  `sort` int(11) NOT NULL,
  `author` varchar(255) NOT NULL DEFAULT 'Pasha P',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`id`, `id_parent`, `name`, `link`, `icon`, `status`, `created_at`, `updated_at`, `sort`, `author`) VALUES
(1, 0, 'Панель управления', 'index/index', 'fa-tachometer', 1, 1447609098, 0, 0, 'Pasha P'),
(2, 0, 'Пользователи', NULL, 'fa-users', 1, 1447609098, 0, 9, 'Pasha P'),
(3, 2, 'Список пользователей', 'users/index/list', '', 1, 1447612390, 0, 0, 'Pasha P'),
(4, 2, 'Добавить пользователя', 'users/index/add', '', 1, 1447612390, 0, 1, 'Pasha P'),
(5, 1, 'Модули', 'index/module', 'fa-cubes', 1, 1447614857, 0, 0, 'Pasha P'),
(6, 1, 'Статистика', 'index/stat-info', 'fa-bar-chart', 1, 1447911960, 0, 1, 'Pasha P'),
(7, 1, 'Настройки', 'index/config', 'fa-cogs', 1, 1448109948, 0, 2, 'Pasha P'),
(8, 0, 'Каталог', NULL, 'fa-inbox', 1, 1448285759, 0, 1, 'Pasha P'),
(20, 0, 'Туризм', NULL, 'fa-map-signs', 1, 1448384122, 0, 2, 'Pasha P'),
(21, 20, 'Туры', 'tyr/index', 'fa-map-signs', 1, 1448384539, 0, 0, 'Pasha P'),
(23, 20, 'Характеристики тура', 'tyr/specifications', 'fa-barcode', 1, 1448385087, 0, 2, 'Pasha P'),
(24, 20, 'Настройки', 'tyr/config', 'fa-cog', 1, 1448385087, 0, 3, 'Pasha P');

-- --------------------------------------------------------

--
-- Структура таблицы `module`
--

CREATE TABLE IF NOT EXISTS `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `ico` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `create_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `write` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `module`
--

INSERT INTO `module` (`id`, `name`, `active`, `ico`, `description`, `create_at`, `updated_at`, `write`) VALUES
(1, 'Index', 1, 'fa-info-circle', '', 1447499594, 0, 0),
(2, 'Ajax', 1, 'fa-bolt', '', 1447499594, 0, 0),
(3, 'Articles', 1, 'fa-book', '', 1447499781, 0, 1),
(4, 'Catalog', 1, 'fa-folder-open-o', '', 1447499781, 0, 1),
(5, 'User', 1, 'fa-users', '', 1447500335, 0, 0),
(6, 'Tyr', 1, 'fa-map-signs', '', 1448383502, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tyr`
--

CREATE TABLE IF NOT EXISTS `tyr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `status` int(1) NOT NULL,
  `create_at` int(10) NOT NULL,
  `modif_at` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tyr_specifications`
--

CREATE TABLE IF NOT EXISTS `tyr_specifications` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `status` int(1) NOT NULL,
  `type` varchar(32) DEFAULT NULL COMMENT 'тип поля(input,select)',
  `create_at` int(10) NOT NULL,
  `modif_at` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tyr_specif_type`
--

CREATE TABLE IF NOT EXISTS `tyr_specif_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `alias` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `tyr_specif_type`
--

INSERT INTO `tyr_specif_type` (`id`, `name`, `alias`) VALUES
(1, 'Однострочное текстовое поле', 'input'),
(2, 'Текстовое поле', 'textarea'),
(3, 'Выбор значения из списка', 'select'),
(4, 'Радиокнопка', 'radio'),
(5, 'Текстовое поле с редактором', 'tiny'),
(6, 'Поле дата', 'data'),
(7, 'поле промежуток дат', 'data_of'),
(8, 'поле множественных дат', 'data_sell'),
(9, 'Множественный выбор значения из списка', 'select_multi');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `login` varchar(128) NOT NULL,
  `pass` varchar(128) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `create_at` int(10) NOT NULL,
  `updated_at` int(10) NOT NULL,
  `role_id` int(3) NOT NULL DEFAULT '0',
  `blocked` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `pass`, `hash`, `create_at`, `updated_at`, `role_id`, `blocked`) VALUES
(2, 'admin', 'seu.8HpuOxyTU', 'b7e661cb0955323edd7550de7545a3b4', 1447501569, 0, 0, 0);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `configure`
--
ALTER TABLE `configure`
  ADD CONSTRAINT `configure_ibfk_1` FOREIGN KEY (`type`) REFERENCES `configure_types` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `configure_ibfk_2` FOREIGN KEY (`group`) REFERENCES `configure_groups` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tyr_specifications`
--
ALTER TABLE `tyr_specifications`
  ADD CONSTRAINT `tyr_specifications_ibfk_1` FOREIGN KEY (`type`) REFERENCES `tyr_specif_type` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
