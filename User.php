<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 28.10.2015
 * Time: 17:53
 */

namespace Core;

use Core\QB\DB;
class User
{
    static $_instance;
    public $User_status;
    public $Users;
    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }

    public function getUser($login,$pass)
    {
        $key = $this->getKey();
        $pass_key = crypt($pass,$key);
        $hash = md5($login.$pass_key.$key);
        //var_dump($hash);
        $result = DB::select()
            ->from('user')
            //->join('config_groups')->on('config.group', '=', 'config_groups.alias')
            ->where('login', '=', $login)
            ->where('pass', '=', $pass_key)
            ->where('hash', '=', $hash)
            ->find_all();
        if(count($result)>0){
            $this->Users->id = $result[0]->id;
            $this->Users->login = $result[0]->login;
            $this->Users->blocked = $result[0]->blocked;
            $this->Users->hash = $result[0]->hash;
            $this->Users->role = $result[0]->role_id;
            $_SESSION['User'] = $this->Users->login;
            $_SESSION['Hash'] = $result[0]->hash;
            return true;
        }else{
            return false;
        }
    }
    private function accessUser($login,$hash)
    {
        $result = DB::select()
            ->from('user')
            ->where('login', '=', $login)
            ->where('hash', '=', $hash)
            ->find_all();
        if(count($result)>0){
            $this->Users->id = $result[0]->id;
            $this->Users->login = $result[0]->login;
            $this->Users->blocked = $result[0]->blocked;
            $this->Users->hash = $result[0]->hash;
            $this->Users->role = $result[0]->role_id;
             return true;
        }else{
            return false;
        }
    }
    private function getKey(){
        $result = DB::select('key_val')
            ->from('config')
            ->where('key_name', '=', 'user_key')
            ->execute();
        return $result[0]['key_val'];
    }
    public function User_Status(){
        $this->User_status = false;
        if(isset($_SESSION['Auth']) and isset($_SESSION['User']) and isset($_SESSION['Hash']) and $_SESSION['User'] != '' and $_SESSION['Hash'] != '')
        {
            $login = Arr::get($_SESSION, 'User');
            $hash = Arr::get($_SESSION, 'Hash');
            if($this->accessUser($login,$hash)){
                $this->User_status = true;
            }
        }
        return $this->User_status;
    }

}