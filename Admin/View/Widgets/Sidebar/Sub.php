<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 17.11.2015
 * Time: 8:16
 */

?>
<ul class="subMenu" <?php echo $check ? 'style="height:'.(count($menu[$obj->id]) * 29).'px;"' : NULL; ?>>
    <?php foreach ( $obj->subMenu as $_obj ): ?>
        <?php $check = false; ?>
        <?php if( $arr[1].'/'.$arr[2] == $_obj->link || (!isset($arr[2]) && $arr[1].'/index' == $_obj->link) || ($arr[2] == 'edit' && $arr[1].'/index' == $_obj->link) ): ?>
            <?php $check = true; ?>
        <?php endif; ?>
        <li class="<?php echo $_obj->check ? 'cur' : ''; ?>">
            <a class="<?php echo $_obj->check ? 'cur' : ''; ?>" href="/admin/<?php echo $_obj->link; ?>">
                <i class="fa <?php echo $_obj->check ? 'fa-caret-right' : 'fa-circle'; ?>"></i>
                <?php echo $_obj->name; ?>
                <?php if( isset($counts) && is_array($counts) && array_key_exists($_obj->count, $counts) && (int) $counts[$_obj->count] > 0 ): ?>
                    <span class="pull-right"><?php echo (int) $counts[$_obj->count]; ?></span>
                <?php endif; ?>
            </a>
        </li>
    <?php endforeach ?>
</ul>
