<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 20.11.2015
 * Time: 8:33
 */?>
<?php /*//if( \Core\User::caccess() != 'edit' ): ?>
    <?php if ($status == 1): ?>
        <i class="fa fa-check-square-o"></i>
    <?php else: ?>
        <i class="fa fa-dot-circle-o red"></i>
    <?php endif ?>
<?php //endif; */?>
<?php //if( \Core\User::caccess() == 'edit' ): ?>
    <a
        data-pub="<?php echo $toltip['yes'];?>"
        data-unpub="<?php echo $toltip['no'];?>"
        title="<?php echo $status == 1 ? $toltip['yes'] : $toltip['no']; ?>"
        data-status="<?php echo $status; ?>"
        data-id="<?php echo $id; ?>"
        href="#"
        class="setStatus bs-tooltip btn btn-xs <?php echo $status == 1 ? 'btn-success' : 'btn-danger' ?>"
        >
        <?php if ($status == 1): ?>
            <i class="fa fa-check-square-o"></i>
        <?php else: ?>
            <i class="fa fa-dot-circle-o"></i>
        <?php endif ?>
    </a>
<?php //endif; ?>
