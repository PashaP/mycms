<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 29.11.2015
 * Time: 15:13
 */
?>
<div class="form-group">
    <label class="control-label" for="field_<?php echo Core\Text::translit($obj->name); ?>"><?php echo $obj->title; ?></label>
        <?php if($obj->type == 'textarea'): ?>
        <div class="">
            <textarea id="field_<?php echo Core\Text::translit($obj->name); ?>" name="<?php echo Core\Text::translit($obj->name); ?>" rows="5" class="form-control valid"><?php echo $obj->value; ?></textarea>
        </div>
        <?php elseif($obj->type == 'tiny'): ?>
        <div class="">
            <textarea id="field_<?php echo Core\Text::translit($obj->name); ?>" name="<?php echo Core\Text::translit($obj->name); ?>" rows="5" class="tiny"><?php echo $obj->value; ?></textarea>
        </div>
        <?php elseif($obj->type == 'select'): ?>
            <?php $values = explode(',',$obj->set_val);?>
            <?php if(!$values): ?>
                <?php $values = array(); ?>
            <?php endif; ?>
            <div class="sellect-dop">
                <select id="field_<?php echo Core\Text::translit($obj->name); ?>" class="form-control valid" name="<?php echo Core\Text::translit($obj->name); ?>">
                    <?php foreach($values AS $v):?>
                        <option value="<?php echo $v; ?>" <?php echo $obj->value == $v ? 'selected' : NULL; ?>><?php echo $v; ?></option>
                    <?php endforeach; ?>
                </select>
                <div class="funck-dop">
                    <i data-title="добавить к списку" class="fa fa-plus-square bs-tooltip sell-add"></i>
                    <i data-title="удалить выбранный элемент из списка" class="fa fa-minus-square bs-tooltip sell-minus"></i>
                    <i data-title="отсортировать список" class="fa fa-sort-alpha-asc  bs-tooltip sell-sort"></i>
                </div>
                <input id="val_<?php echo Core\Text::translit($obj->name); ?>" type="hidden" name="valSell" value="<?php echo (implode(',',$values)); ?>">
            </div>
            <script>
                $('.sell-add').on('click', function(){
                    var htm = '' +
                        '<div class="form-groups">' +
                            '<span style="font-size: small;margin-left: 80px;">Новое значение</span>' +
                            '<input type="text" name="selAddname" placeholder="ввод имени нового значения списка" class="inp_beck" onkeyup="new_press(event);">' +
                            '<span class="btn btn-add-sell bs-tooltip" data-title="добавить к списку и не закрывая создать новый" style="margin-left: 10px;" onclick="btn_add_sell_new();">Вставить и добавить</span>' +
                            '<span class="btn btn-add-sell bs-tooltip" data-title="добавить к списку и закрыть окно добавления" onclick="btn_add_sell();">Вставить</span>' +
                        '</div>';
                    $('.beck .form-groups').html(htm);
                    $('.beck').show();

                });
                $('.sell-minus').on('click', function(){
                    var sell = $('select#field_<?php echo Core\Text::translit($obj->name); ?>')[0];
                    sell.options[sell.selectedIndex] = null;
                    //alert('елемент удален из списка');
                });
                $('.sell-sort').on('click', function(){
                    var sell = $('select#field_<?php echo Core\Text::translit($obj->name); ?>')[0];
                    var Val = $('input[name="valSell"]')[0];
                    var opt = sell.options;
                    var opt_arr = new Array();
                    for(var i=0; i<opt.length;i++){
                        opt_arr[i]=opt[i].value;
                    }
                    opt_arr.sort();
                    Val.value = opt_arr.join();
                    sell.options.length = 0;
                    for(var i=0; i<opt_arr.length;i++){
                        sell.options[i] = new Option(opt_arr[i], opt_arr[i]);
                    }
                });
                function btn_add_sell(){
                    var sell = $('select#field_<?php echo Core\Text::translit($obj->name); ?>')[0];
                    var name = $('input[name="selAddname"]')[0];
                    sell.options[sell.options.length] = new Option(name.value, name.value);
                    name.value ='';
                    new_add();
                    $('.beck').hide();
                }
                function btn_add_sell_new(){
                    var sell = $('select#field_<?php echo Core\Text::translit($obj->name); ?>')[0];
                    var name = $('input[name="selAddname"]')[0];
                    sell.options[sell.options.length] = new Option(name.value, name.value);
                    name.value ='';
                    new_add();
                }
                function new_add(){
                    var sell = $('select#field_<?php echo Core\Text::translit($obj->name); ?>')[0].options;
                    var Val = $('input[name="valSell"]')[0];
                    var param = new Array();
                    for(i=0;i<sell.length;i++){
                        param[param.length] = sell[i].value;
                    }
                    param.sort();
                    Val.value = param.join();
                }
                function new_press (e) {
                    if(e.keyCode == 13){
                        btn_add_sell_new();
                    }
                    if(e.keyCode == 27){
                        $('.beck').hide();
                    }
                    //console.log(e.keyCode);
                }

            </script>

        <?php elseif($obj->type == 'radio'): ?>
            <?php $values = json_decode($obj->set_val, true); ?>
            <?php if(!$values): ?>
                <?php $values = array('0'=> array('key'=>'да','value'=>'да'),'1'=> array('key'=>'нет','value'=>'нет')); ?>
            <?php endif; ?>
        <div class="">
            <div class="controls">
                <?php foreach($values AS $v): ?>
                    <label class="checkerWrap-inline radioWrap col-md-4" style="margin-right: 0;">
                        <input name="<?php echo Core\Text::translit($obj->name); ?>" value="<?php echo $v['value']; ?>" type="radio" <?php echo $obj->value == $v['value'] ? 'checked' : ''; ?> class="valid">
                        <?php echo $v['key']; ?>
                    </label>
                <?php endforeach; ?>
            </div>
        </div>
        <?php elseif($obj->type == 'password'): ?>
        <div class="">
            <input id="field_<?php echo Core\Text::translit($obj->name); ?>" autocomplete="off" class="form-control valid" type="password" name="<?php echo Core\Text::translit($obj->name); ?>" value="<?php echo $obj->value; ?>"/>
            <span class="input-group-btn">
                <button class="btn showPassword" type="button">Показать</button>
            </span>
        </div>
        <?php elseif($obj->type == 'data_of'): ?>
            <div class="">
                <li title="Выберите дату от" class="btn btn-xs bs-tooltip" style="display: inline-flex;">
                    <i class="data-lab">дата от</i>
                    <input name="dataOt" class="form-control valid myPicker" type="text" id="data_of_data_ot_<?php echo $obj->id;?>" value="<?php echo date('d.m.Y'); ?>" >
                    <i class="fa fa-calendar"></i>
                </li>
                <li title="Выберите дату до" class="btn btn-xs bs-tooltip" style="display: inline-flex;">
                    <i class="data-lab">дата до</i>
                    <input name="dataTo" class="form-control valid myPicker" type="text" id="data_of_data_to_<?php echo $obj->id;?>" value="<?php echo date('d.m.Y'); ?>" >
                    <i class="fa fa-calendar"></i>
                </li>
            </div>
            <script>
                $(function(){
                    var pickerInit = function( selector ) {
                        var date = $(selector).val();
                        $(selector).datepicker({
                            showOtherMonths: true,
                            selectOtherMonths: false
                        });
                        $(selector).datepicker('option', $.datepicker.regional['ru']);
                        var dateFormat = $(selector).datepicker( "option", "dateFormat" );
                        $(selector).datepicker( "option", "dateFormat", 'dd.mm.yy' );
                        $(selector).val(date);
                    };;
                    pickerInit('.myPicker');
                });
            </script>
        <?php elseif($obj->type == 'data'): ?>
        <div class="">
            <li title="Выберите дату" class="btn btn-xs bs-tooltip" style="display: inline-flex;">
                <i class="data-lab">дата</i>
                <input name="dataOt" class="form-control valid myPicker" type="text" id="data_<?php echo $obj->id;?>" value="<?php echo date('d.m.Y'); ?>" >
                <i class="fa fa-calendar"></i>
            </li>
        </div>
            <script>
                $(function(){
                    var pickerInit = function( selector ) {
                        var date = $(selector).val();
                        $(selector).datepicker({
                            showOtherMonths: true,
                            selectOtherMonths: false
                        });
                        $(selector).datepicker('option', $.datepicker.regional['ru']);
                        var dateFormat = $(selector).datepicker( "option", "dateFormat" );
                        $(selector).datepicker( "option", "dateFormat", 'dd.mm.yy' );
                        $(selector).val(date);
                    };;
                    pickerInit('.myPicker');
                });
            </script>
        <?php else: ?>
        <div class="">
            <input id="field_<?php echo Core\Text::translit($obj->name); ?>" class="form-control valid" type="text" name="<?php echo Core\Text::translit($obj->name); ?>" value="<?php echo $obj->value; ?>"/>
        </div>
        <?php endif; ?>

</div>

