<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 23.11.2015
 * Time: 13:49
 */
?>
<div class="Msg" style="display: none;">
    <span id="msg-icon" class="<?php echo $class;?>"><i class="fa <?php echo $ico; ?>"></i></span>
    <span class="title <?php echo $class;?>"><?php echo $title; ?></span>
    <span class="text"><?php echo $text; ?></span>
</div>
<script>
    $(document).ready ( function(){
        var times = setTimeout(show, 1000);

        function show(){
            $('.Msg').show();
            times = setTimeout(hide, 5000);
        }
        function hide(){
            $('.Msg').hide();
        }
    });
</script>
<style>
    .Msg{
        position: absolute;
        right: 10px;
        bottom: 100px;
        background: #F9F9F9;
        padding: 5px;
        box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.52);
        border: 1px solid #D9D9D9;
    }
    #msg-icon{
        position: absolute;
        line-height: 30px;
        font-size: large;
        margin-left: 10px;

    }

    .Msg > .title{
        display: block;
        background: #E4DDDD;
        color: white;
        text-shadow: 0px 0px 5px rgba(0, 0, 0, 0.82);
        padding: 5px;
        text-align: center;
        font-weight: 800;
    }
    .Msg > .text{
        display: block;
        padding: 5px;
    }
    #msg-icon.err{
        color: red;
        text-shadow: 0px 0px 5px rgba(237, 251, 6, 0.52);
    }
    .Msg > .title.err{background: #EAA2A2;}
    #msg-icon.ok{
        color: #035406;
    }
    .Msg > .title.ok{background: #62E267;}

</style>
