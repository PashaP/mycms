<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 15.11.2015
 * Time: 13:28
 */
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="">
<meta name="keywords" content="">
<meta http-equiv="imagetoolbar" content="no">
<title><?php echo $html->title; ?></title>

<link rel="stylesheet" href="/Admin/Media/css/style.css">
<link rel="stylesheet" href="/Admin/Media/css/ui-lightness/jquery-ui-1.10.1.min.css">
<link rel="stylesheet" href="/Admin/Media/css/liValidForm.css">
<link rel="stylesheet" href="/Admin/Media/css/magnific-popup.css">
<link rel="stylesheet" href="/Admin/Media/css/font-awesome.css">
<link rel="stylesheet" href="/Admin/Media/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" href="/Admin/Media/css/daterangepicker-bs3.css">
<link rel="stylesheet" href="/Admin/Media/css/liTabs.css">
<link rel="stylesheet" href="/Admin/Media/css/liQuickEdit.css">
<link rel="stylesheet" href="/Admin/Media/css/liHarmonica.css">
<link rel="stylesheet" href="/Admin/Media/css/jquery.minicolors.css">

<link rel="stylesheet" href="/Admin/Media/css/nestable.css">

<link rel="stylesheet" href="/Admin/Media/css/typeahead.css">
<link rel="stylesheet" href="/Admin/Media/css/tagsinput.css">
<link rel="stylesheet" href="/Admin/Media/css/iphone-style-checkboxes.css">
<link rel="stylesheet" href="/Admin/Media/css/jquery.dataTables.css">
<link rel="stylesheet" href="/Admin/Media/css/my.css">
<link rel="stylesheet" href="/Admin/Media/css/msg.css">

<!-- <link rel="stylesheet" href="/Admin/Media/css/sresponsiveness.css"> -->


<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<!-- <meta name="viewport" content="target-densitydpi=device-dpi"> -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<link rel="shortcut icon" href="/Media/favicons/favicon.ico">
<link rel="image_src" href="/Admin/Media/pic/expres_icon.jpg">
<link rel="apple-touch-icon" sizes="57x57" href="/Media/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/Media/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/Media/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/Media/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/Media/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/Media/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/Media/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/Media/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/Media/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/Media/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/Media/favicons/favicon-194x194.png" sizes="194x194">
<link rel="icon" type="image/png" href="/Media/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/Media/favicons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/Media/favicons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/Media/favicons/manifest.json">
<link rel="mask-icon" href="/Media/favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="apple-mobile-web-app-title" content="PlatP cms">
<meta name="application-name" content="PlatP cms">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">

<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--<script src="/Admin/Media/js/wysihtml5.min.js"></script> -->


<script src="/Admin/Media/js/modernizr.js"></script>
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
<script>window.jQuery || document.write('<script src="/Admin/Media/js/jquery-1.9.0.min.js">\x3C/script>')</script>
<script src="/Admin/Media/js/jquery-ui-1.10.1.min.js"></script>
<script src="/Admin/Media/js/i18n/jquery-ui-i18n.js"></script>
<!--<script src="/Admin/Media/js/bootstrap.min.js"></script> -->
<script src="/Admin/Media/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Admin/Media/js/jquery.liActualSize.js"></script>
<script src="/Admin/Media/js/liValidForm.js"></script>
<script src="/Admin/Media/js/jquery.cookie.min.js"></script>
<script src="/Admin/Media/js/jquery.mousewheel.js"></script>
<script src="/Admin/Media/js/jquery.mCustomScrollbar.js"></script>
<script src="/Admin/Media/js/moment.js"></script>
<script src="/Admin/Media/js/daterangepicker.js"></script>
<script src="/Admin/Media/js/jquery.liTip.js"></script>
<script src="/Admin/Media/js/jquery.liTabs.js"></script>
<script src="/Admin/Media/js/jquery.liHarmonica.js"></script>
<script src="/Admin/Media/js/jquery.nestable.min.js"></script>
<script src="/Admin/Media/js/typeahead.min.js"></script>
<script src="/Admin/Media/js/jquery.tagsinput.min.js"></script>
<script src="/Admin/Media/js/iphone-style-checkboxes.js"></script>
<script src="/Admin/Media/js/jquery.dataTables.min.js"></script>
<script src="/Admin/Media/js/dataTables.bootstrap.js"></script>
<!--<script src="--><!--"></script>-->
<script src="/Admin/Media/js/highcharts.js"></script>
<script src="/Admin/Media/js/data.js"></script>
<script src="/Admin/Media/js/jquery.liQuickEdit.js"></script>
<script src="/Admin/Media/js/jquery.magnific-popup.min.js"></script>
<!-- <script src="http://maps.google.com/maps/api/js?sensor=false"></script> -->
<script src="/Admin/Media/js/jquery.minicolors.js"></script>

<script src="/Admin/Media/js/tinymce/tinymce.min.js"></script>

<script src="/Admin/Media/js/main.js"></script>
<script src="/Admin/Media/js/plugins.js"></script>

<script src="/Admin/Media/js/ui.js"></script>

<script src="/Admin/Media/js/my.js"></script>

<script src="/Admin/Media/js/core-function.js"></script>


<!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->


<link rel="stylesheet" href="/Admin/Media/js/lightbox/css/lightbox.css">
<script src="/Admin/Media/js/lightbox/lightbox.min.js"></script>

<script>
    $(function(){
        if($('.tinymceEditor').length){
            tinymce.init({
                selector: "textarea.tinymceEditor",
                skin : "Admin",
                language : 'ru',
                plugins: [
                    "advlist autolink lists link image charmap print preview hr",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons paste textcolor colorpicker textpattern responsivefilemanager"
                ],
                toolbar1: "undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager",
                image_advtab: true,
                external_filemanager_path:"/Admin/Media/js/tinymce/filemanager/",
                filemanager_title:"Менеджер файлов" ,
                external_plugins: { "filemanager" : "filemanager/plugin.min.js"},
                document_base_url: "http://"+window.location.hostname+"/",
                convert_urls: false,
                relative_urls: false,
                default_language:'ru'
            });

            var wLastSmall, wLastBig;
            if($(window).width() > 700){
                wLastSmall = false;
                wLastBig = true;
            }else{
                wLastSmall = true;
                wLastBig = false;
            }

            $(window).on('resize',function(){

                if($(window).width() > 700 && wLastSmall){
                    wLastSmall = false;
                    wLastBig = true;
                    parent.tinyMCE.activeEditor.windowManager.close(window);
                }
                if($(window).width() < 700 && wLastBig){
                    wLastSmall = true;
                    wLastBig = false;
                    parent.tinyMCE.activeEditor.windowManager.close(window);
                }
            })
        }
    });
</script>

