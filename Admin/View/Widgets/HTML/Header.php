<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 15.11.2015
 * Time: 13:44
 */
?>
<header class="navbar navbarPos">
    <div class="clearFix navbarIndent">
        <a class="fontLogo" href="/admin/index">
            <span class="top fontLogoName">PlatP</span>
            <span class="top fontLogoTitle">
                <span class="fontLogoLabel">cms</span>
                <span class="fontLogoVersion">1.0</span>
            </span>
        </a>
        <a title="Переключить навигацию" class="tip toggleSidebar" href="#">
            <i class="fa fa-bars"></i>
        </a>
        <ul class="navbarNav">

            <li class="dropdown dropdownMenuHidden">
                <a class="dropdownToggle" href="#">
                    <i class="fa fa-male"></i>
                    <span class="navText username"><?php echo Core\User::factory()->Users->login; ?></span>
                    <i class="fa fa-caret-down small"></i>
                </a>
                <ul class="dropdownMenu pull-right">
                    <li>
                        <a href="/admin/auth/edit">
                            <i class="fa fa-user"></i>
                            Профиль
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/admin/auth/logout">
                            <i class="fa-key"></i>
                            Выйти
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</header>
