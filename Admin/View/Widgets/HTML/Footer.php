<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 15.11.2015
 * Time: 13:41
 */
$max_mem = ini_get("memory_limit");
if($max_mem == '-1') $max_mem = 'full';
$tek_mem = round(((memory_get_peak_usage(true)/1024)/1024),2);

?>
<div class="rowSection footer">
    <div class="contentSection">

        <div class="admin-infos hidden">
            <div id="btn-infos-admin"  class="tip" data-title="Информационная панель админа(время,память,кол админов)"><i class="fa fa-caret-square-o-right"></i></div>
            <div class="infos-content">
                <div class="tip" data-title="Количество Админов/в сети">
                    <i class="fa fa-users"></i><span>1/1</span>
                </div>
                <div class="tip" data-title="Обьем памяти  доступный/потребовавшийся <?php if($max_mem == 'full') echo '(full означает не ограничено)'?>">
                    <i class="fa fa-tachometer"></i><span><?php echo $max_mem.'/'.$tek_mem.' mb';  ?></span>
                </div>
                <div class="tip" data-title="Время создания страницы">
                    <i class="fa fa-clock-o"></i><span> <?php echo round((microtime(true) - START_TIME),4).' сек.'?></span>
                </div>
            </div>
        </div>
        <div class="copyWrap">
            © 2015 <a target="_blank" href="#" style="color: white;text-shadow: 0px 0px 8px black;">PlatP CMS 1.0</a>
        </div>
    </div>
</div>
<script>
    $('#btn-infos-admin').click(function () {
        if($(".admin-infos").hasClass("hidden")){
            $(".admin-infos").removeClass("hidden");
            $(".admin-infos").addClass("active");
        }else{
            $(".admin-infos").removeClass("active");
            $(".admin-infos").addClass("hidden");
        }
    });
</script>
