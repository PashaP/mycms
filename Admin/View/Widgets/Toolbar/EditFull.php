<div class="toolbar padding">
    <div class="btn-group">
        <a title="Сохранить" href="#" data-value="save" class="btn btn-lg text-success bs-tooltip btn-save">
            <i class="fa fa-check"></i> <span class="hidden-xx">Сохранить</span>
        </a>
        <a title="Сохранить и закрыть" data-value="save-close" href="#" class="btn btn-lg text-warning bs-tooltip btn-save">
            <i class="fa fa-check"></i> <span class="hidden-xx">Сохранить и закрыть</span>
        </a>
        <a title="Сохранить и добавить еще" data-value="save-add" href="#" class="btn btn-lg text-info bs-tooltip btn-save">
            <i class="fa fa-check"></i> <span class="hidden-xx">Сохранить и добавить еще</span>
        </a>
        <script>
            $('.toolbar').on('click', '.btn-save', function(e){
                e.preventDefault();
                var val = $(this).data('value');
                var button = $('form#myForm input[type="submit"]');
                button.after('<input name="button" type="hidden" value="' + val + '" />');
                //button.click();
            });
        </script>
        <a title="Закрыть" data-value="exit" href="#<?php //echo $list_link ? $list_link : '/admin/' . Core\Route::controller() . '/index'; ?>" class="btn btn-lg text-danger bs-tooltip">
            <i class="fa fa-times-circle"></i> <span class="hidden-xx">Закрыть</span>
        </a>
    </div>
</div>
<script>
    $(function(){
        var toolbar =  {
            form :$('#myForm')[0],
            url:'', data:'',type:'',
            initialize : function(){
                this.url = $('#ajax').val();
                //alert('Init toolbar');
            },
            send : function(){
                form = $('#myForm');
                this.data = form.find('input,textarea,select').serialize();
                //this.data = $( "#myForm input, textarea, select" ).serialize();
                $.ajax({
                    url: this.url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        data: this.data,
                        btn_type: this.type
                    },
                    beforeSend: function(){
                        toolbar.showOverlayWait($('body'));
                        timeout = setTimeout(function(){
                            toolbar.showOverlayWait($('body'));
                        },200);
                    },
                    comlete: function(){
                        clearTimeout(timeout);
                        toolbar.hideOverlayWait();
                    },
                    success: function(data){
                        clearTimeout(timeout);
                        toolbar.hideOverlayWait();
                        if(data.success){
                            if(data.msg){
                                $('.Msg #msg-icon').html('<i class="fa '+data.msg.ico+'"></i>');
                                $('.Msg .title').addClass('err').html(data.msg.title);
                                $('.Msg .text').addClass('err').html(data.msg.text);
                                $('.Msg').show().addClass('show');
                                var m_timer = setTimeout(function(){
                                    $('.Msg').removeClass('show').hide();
                                }, data.msg.time);
                                if(data.redirect){
                                    var r_timer = setTimeout(function(){
                                        window.location.href = data.redirect;
                                    }, data.msg.time+1000);
                                }
                            }else{
                                if(data.redirect){
                                    window.location.href = data.redirect;
                                }
                            }
                        }
                        /*var html = '<option value="0">Нет</option>';
                        for(var i = 0; i < data.options.length; i++) {
                            html += '<option value="' + data.options[i].id + '">' + data.options[i].name + '</option>';
                        }
                        $('#model_id').html(html);*/
                    },
                    error: function(data){
                        clearTimeout(timeout);
                        toolbar.hideOverlayWait();
                        alert('Error' + data);
                    }
                });
            },
            save : function(){
                this.type = 'save';
                this.send();
            },
            save_close : function(){
                this.type = 'save-close';
                this.send();
            },
            save_add : function(){
                this.type = 'save-add';
                this.send();
            },
            exit : function(){
                this.type = 'exit';
                this.send();
            },
            showOverlayWait : function(element){
                if(!$('#overlayWait').length){
                    element.prepend($('<div id="overlayWait"></div>'));
                }
            },
            hideOverlayWait : function(){
                if($('#overlayWait').length){
                    $('#overlayWait').remove();
                }
            }
        }

        toolbar.initialize();
        $("a[data-value='save']").click(function () {toolbar.save();});
        $("a[data-value='save-close']").click(function () {toolbar.save_close();});
        $("a[data-value='save-add']").click(function () {toolbar.save_add();});
        $("a[data-value='exit']").click(function () {toolbar.exit();});
    });
</script>
