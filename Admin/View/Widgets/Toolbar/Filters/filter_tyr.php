<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 25.11.2015
 * Time: 7:59
 */
?>
<div class="widgetHeader" style="padding-bottom: 10px;">
<?php  ?>
    <form class="widgetContent" action="/wezom/<?php echo Core\Route::controller(); ?>/index" method="get">
        <div class="col-md-2">
            <label class="control-label">Наименование</label>
            <div class="">
                <div class="controls">
                    <input name="name" class="form-control" value="<?php echo Core\Arr::get($_GET, 'name', NULL); ?>">
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <label class="control-label">Статус</label>
            <div class="">
                <div class="controls">
                    <select name="status" class="form-control">
                        <option value="">Все</option>
                        <option value="0" <?php echo Core\Arr::get($_GET, 'status', 2) == '0' ? 'selected' : ''; ?>>Неопубликованы</option>
                        <option value="1" <?php echo Core\Arr::get($_GET, 'status') == '1' ? 'selected' : ''; ?>>Опубликованы</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <label class="control-label">Выводить по</label>
            <div class="">
                <div class="controls">
                    <select name="limit" class="form-control">
                        <?php for($i = 1; $i <= 5; $i++): ?>
                            <?php $number = $i * Core\Config::get('basic.limit_backend'); ?>
                            <option value="<?php echo $number; ?>" <?php echo Core\Arr::get($_GET, 'limit', Core\Config::get('basic.limit_backend')) == $number ? 'selected' : ''; ?>><?php echo $number; ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-1">
            <label class="control-label" style="height:13px;"></label>
            <div class="">
                <div class="controls">
                    <input type="submit" class="btn btn-primary" value="Подобрать" />
                </div>
            </div>
        </div>
        <div class="col-md-1">
            <label class="control-label" style="height:19px;"></label>
            <div class="">
                <div class="controls">
                    <a href="/wezom/<?php echo Core\Route::controller(); ?>/index">
                        <i class="fa-refresh"></i>
                        <span class="hidden-xx">Сбросить</span>
                    </a>
                </div>
            </div>
        </div>
    </form>
    <?php ?>
</div>

