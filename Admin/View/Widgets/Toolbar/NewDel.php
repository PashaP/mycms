<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 26.11.2015
 * Time: 6:59
 */
?>
<div class="toolbar no-padding">
    <div class="btn-group">
        <?php //if ( $add ): ?>
            <a href="/admin/<?php echo $link_add ?>" class="btn btn-lg">
                <i class="fa fa-plus"></i> Создать
            </a>
        <?php //endif ?>
        <?php //if ( $delete ): ?>
            <a title="Удалить" href="/admin/<?php echo Core\Route::controller(); ?>/del" class="btn btn-lg text-warning bs-tooltip delete-items">
                <i class="fa fa-fixed-width">&#xf00d;</i> <span class="hidden-xx">Удалить</span>
            </a>
        <?php //endif ?>
    </div>
</div>
<script>
    $('.toolbar').on('click', '.btn-save', function(e){
        e.preventDefault();
        var val = $(this).data('value');
        var button = $('form#myForm input[type="submit"]');
        button.after('<input name="button" type="hidden" value="' + val + '" />');
        button.click();
    });
</script>
