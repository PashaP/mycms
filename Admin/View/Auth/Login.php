<div class="auLogin">
    <span class="icon-user"></span>
    <input type="text" id="login" class="loginFormEl formIconLogin" autocomplete="off" autofocus placeholder="Логин">
</div>
<div class="auPass">
    <span class="icon-lock22"></span>
    <input type="password" id="password"  class="loginFormEl formIconPass" autocomplete="off" placeholder="Пароль">
</div>
<div class="error"></div>
<div class="auRe">
    <label>
        <input type="checkbox" id="remember">
        <span class="auReLabel">Запомнить меня</span>
    </label>
</div>

<div class="btnWrap clearFix">
    <!-- <a href="#" class="passLink">Забыли пароль?</a> -->
    <a href="#" class="enterLink" id="enterLink">Войти</a>
</div>

<script>
    $(function(){
        $('.auWrap').on('keydown', function(event) {
            if(event.keyCode === 13) {
                $('#enterLink').trigger('click');
            }
        });
        $('#enterLink').on('click', function(e){
            e.preventDefault();
            var login = $('#login').val();
            var password = $('#password').val();
            $('#login').removeClass("err");
            $('#password').removeClass("err");
            var remember = 0;
            if( $('#remember').prop('checked') ) {
                remember = 1;
            }
            $.ajax({
                url: '/admin/ajax/login',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    login: login,
                    password: password,
                    remember: remember
                },
                success: function(data){
                    if( data.success ) {
                        $('.Msg > .title')[0].innerText = data.title;
                        $('#msg-icon').removeClass(Auth.Icon);
                        Auth.Icon = data.ico;
                        $('#msg-icon').addClass(Auth.Icon);
                        Auth.Tmr = data.time;
                        Auth.Error=0;
                        Auth.Acess();
                    } else {
                        $('.Msg > .title')[0].innerText = data.title;
                        $('.Msg > .text')[0].innerText = data.text;
                        $('#msg-icon').removeClass(Auth.Icon);
                        Auth.Icon = data.ico;
                        $('#msg-icon').addClass(Auth.Icon);
                        Auth.Tmr = data.time;
                        Auth.Param = data.param;
                        Auth.Message();
                    }
                }
            });
        });
    });
    var Auth = {
        Msg:"",
        Error:1,
        Icon:'',
        Param:0,
        Tmr:3000,

        Acess : function(){
            $('.Msg > .title').removeClass("err");
            html = '<img class="login-in" src="/Admin/Media/pic/login_open.gif">';
            $('.Msg > .text')[0].innerHTML = html;
            $('.Msg').show();
            $(".Msg").addClass("show");
            var times = setTimeout(Auth.HideMsg, Auth.Tmr);
        },
        Message : function(){
            switch(Auth.Param) {
                case 1:
                    $('#login').addClass("err");
                    break;

                case 2:
                    $('#password').addClass("err");
                    break;
                case 3:
                    $('#login').addClass("err");
                    $('#password').addClass("err");
                    break;
            }
            $('.Msg').show();
            $(".Msg").addClass("show");
            var times = setTimeout(Auth.HideMsg, Auth.Tmr);

        },
        HideMsg : function(){
            $(".Msg").removeClass("show");
            if(Auth.Error == 0)
                Auth.Reloads();
        },
        Reloads : function(){
            window.location.reload();
        },
    }


</script>

<div class="Msg" style="display: none;">
    <span id="msg-icon"></span>
    <span class="title err"></span>
    <span class="text"></span>
</div>
<div class="beck" style="display: none"></div>