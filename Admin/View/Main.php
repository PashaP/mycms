<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 08.11.2015
 * Time: 13:09
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo Core\View::tpl(array('seo' => $_seo, 'html' => $html,),'Widgets/HTML/Head'); ?>
</head>
<body>
<?php echo Core\View::tpl(array('seo' => $_seo, 'html' => $html,),'Widgets/HTML/Header'); ?>
<div class="container fixedHeader">
    <?php echo $menu; ?>
    <div class="contentWrap">
        <div class="contentWrapMar">
            <?php echo Core\View::tpl(array('breadcrumbs' => $_breadcrumbs, 'seo' => $_seo, 'toolbar' => $html->toolbar,),'Widgets/Page-Header'); ?>
            <div class="rowSection">
                <div class="col-md-12">
                    <div class="widget checkbox-wrap">
                        <?php //echo $_filter; ?>
                        <div class="widgetContent">
                            <?php echo $html->content; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo Core\View::tpl(array('seo' => $_seo, 'doc' => $html,),'Widgets/HTML/Footer'); ?>
<?php echo $html->msg;?>
<?php echo Core\View::tpl(array(),'Widgets/HTML/msg_base'); ?>
</body>
</html>
