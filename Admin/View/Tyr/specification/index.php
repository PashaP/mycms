<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 26.11.2015
 * Time: 8:13
 */
?>
<?php echo $module['toolbar']; ?>
<div class="rowSection">
    <div class="col-md-12">
        <div class="widget">
            <?php //echo $filter; ?>
            <div class="widget">
                <div class="widgetContent">
                    <table class="table table-striped table-hover checkbox-wrap">
                        <thead>
                        <tr>
                            <th class="checkbox-head">
                                <label><input type="checkbox"></label>
                            </th>
                            <th>Название</th>
                            <th>Группа</th>
                            <th>Тип</th>
                            <th>Статус</th>
                            <th class="nav-column textcenter">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ( $result as $obj ): //var_dump($obj); ?>
                            <tr data-id="<?php echo $obj->id; ?>">
                                <td class="checkbox-column">
                                    <label><input type="checkbox"></label>
                                </td>
                                <td>
                                    <a href="/admin/<?php echo Core\Route::controller(); ?>/specifications/edit/<?php echo $obj->id; ?>">
                                        <?php echo $obj->name ?>
                                    </a>
                                </td>
                                <td>
                                    <a href="/admin/<?php echo Core\Route::controller(); ?>/specifications/edit/<?php echo $obj->id; ?>">
                                        <?php echo $obj->group_name; ?>
                                    </a>
                                </td>
                                <td>
                                    <a href="/admin/<?php echo Core\Route::controller(); ?>/specifications/edit/<?php echo $obj->id; ?>">
                                        <?php echo $obj->type_name; ?>
                                    </a>
                                </td>
                                <td width="45" valign="top" class="icon-column status-column">
                                    <?php echo Core\View::widget(array( 'status' => $obj->status, 'id' => $obj->id,
                                        'toltip' => array('yes' => '<b>Отключить</b><br>данная характеристика будет выключена' ,
                                                          'no' => '<b>Включить</b><br>данная характеристика будет включена') ), 'StatusList'); ?>
                                </td>
                                <td class="nav-column">
                                    <ul class="table-controls">
                                        <li>
                                            <a title="Управление" class="bs-tooltip dropdownToggle" href="javascript:void(0);"><i class="fa fa-cog size14"></i></a>
                                            <ul class="dropdownMenu pull-right">
                                                <li>
                                                    <a title="Редактировать" href="/admin/<?php echo Core\Route::controller(); ?>/specifications/edit/<?php echo $obj->id; ?>"><i class="fa fa-pencil"></i> Редактировать</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a title="Удалить" onclick="return confirm('Это действие необратимо. Продолжить?');" href="/admin/<?php echo Core\Route::controller(); ?>/specifications/delete/<?php echo $obj->id; ?>"><i class="fa fa-trash-o text-danger"></i> Удалить</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                    <?php echo $pager; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<span id="parameters" data-table="<?php echo $tablename; ?>" data-pole="status"></span>

