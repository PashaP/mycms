<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 13.12.2015
 * Time: 17:50
 */
?>

<form id="myForm" class="rowSection validat" method="post" action="/admin/tyr/specifications/group" enctype="multipart/form-data" data-save = "no">
    <input id="field_id" class="form-control valid" type="hidden" name="id" value="<?php echo $obj->id; ?>"/>
    <input id="field_ret" class="form-control valid" type="hidden" name="ret" value="<?php echo $obj->link; ?>"/>
    <input type="hidden" id="ajax" value="/admin/tyr/specif/groupAjax" >
    <!--div class="form-actions" style="display: none;">
        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
    </div-->
    <div class="form-group">
        <label class="control-label bs-tooltip" for="field_name" data-title="Название обязательно для заполнения">Название</label>
        <div class="">
            <input id="field_name" class="form-control valid" type="text" name="name" value="<?php echo $obj->name; ?>"/>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label bs-tooltip" for="field_name" data-title="Алиас обязательно для заполнения">Алиас</label>
        <div class="">
            <input id="field_alias" class="form-control valid" type="text" name="alias" value="<?php echo $obj->alias; ?>"/>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label bs-tooltip" for="field_group" data-title="для продолжения выберите тип характеристики">Тип</label>
        <div class="">
            <select id="field_group" class="form-control valid" name="group">
                <?php foreach($group as $itm): ?>

                    <option value="<?php echo $itm->alias; ?>" <?php echo $obj->group == $itm->alias ? 'selected' : NULL; ?>><?php echo $itm->name; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label bs-tooltip" for="field_group" data-title="для продолжения выберите тип характеристики">Вариант Выбора</label>
        <div class="sellect-dop">
            <select id="field_<?php echo $obj->alias; ?>" class="form-control valid" name="<?php echo $obj->alias; ?>">
                <?php foreach($values AS $v):?>
                    <option value="<?php echo $v; ?>" <?php echo $obj->value == $v ? 'selected' : NULL; ?>><?php echo $v; ?></option>
                <?php endforeach; ?>
            </select>
            <div class="funck-dop">
                <i data-title="добавить к списку" class="fa fa-plus-square bs-tooltip sell-add"></i>
                <i data-title="удалить выбранный элемент из списка" class="fa fa-minus-square bs-tooltip sell-minus"></i>
                <i data-title="отсортировать список" class="fa fa-sort-alpha-asc  bs-tooltip sell-sort"></i>
            </div>
            <input id="val_<?php echo $obj->alias; ?>" type="hidden" name="valSell" value="<?php echo (implode(',',$values)); ?>">
        </div>
    </div>
    <div class="form-group fields">
        <label class="control-label bs-tooltip" for="field_param" data-title="отметьте нужные характеристики">Характеристики</label>
        <div class="param">
            <?php foreach($specif as $obj_s): ?>
                <div class="group_val">
                    <input id="sell_specif_<?php echo $obj_s->id;?>" class="chek-box-specific" type="checkbox" name="group_val_<?php echo $obj_s->id;?>" value="<?php echo $obj_s->alias;?>" >
                    <label class="control-label bs-tooltip" for="sell_specif_<?php echo $obj_s->id;?>" data-title="отметьте нужные характеристики"><?php echo $obj_s->name;?></label>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <input type="hidden" name="group_sell_val" id="group_sell_val" value="">
</form>

<div class="becks" style="display: none;">
    <div class="form-groups">
        <span style="font-size: small;margin-left: 80px;">Новое значение</span>
        <input type="text" name="selAddname" placeholder="ввод имени нового значения списка" class="inp_beck" onkeyup="new_press(event);">
        <span class="btn btn-add-sell bs-tooltip" data-title="добавить к списку и не закрывая создать новый" style="margin-left: 10px;" onclick="btn_add_sell_new();">Вставить и добавить</span>
        <span class="btn btn-add-sell bs-tooltip" data-title="добавить к списку и закрыть окно добавления" onclick="btn_add_sell();">Вставить</span>
    </div>
    <i class="fa fa-times-circle closed"></i>
</div>
<script>
    $('#field_name').on('change', function(){
            $('#field_name').removeClass('red');
        });
    $('#field_group').on('change', function(){
            if(form_valid()){
                var sel = $('#field_group')[0].value;
                if(sel == 'group'){
                    if(window.location.pathname != '/admin/tyr/specifications/group')
                        window.location = '/admin/tyr/specifications/group';
                }
                if(sel == 'base'){
                    if(window.location.pathname != '/admin/tyr/specifications/add')
                        window.location = '/admin/tyr/specifications/add';
                }
            }
        });
    $('.closed').on('click', function(){
        $('.becks').hide();
    });

    function form_valid(){
        if($('#field_name').val != ''){
            return true;
        }
        $('#field_name').addClass('red');
        return false;
    }
    $('.chek-box-specific').on('change',function(){
        var name = $('#field_<?php echo $obj->alias; ?>')[0].value;
        group.change_val(name);
    });
    $("a[data-value='save']").click(function () {group.saved('save');});
    $("a[data-value='save-close']").click(function () {group.saved('save-close');});
    $("a[data-value='save-add']").click(function () {group.saved('save-add');});
    $("a[data-value='exit']").click(function () {});
    var group = {
            name: new Array(),
            value: new Array(),
            col_val: 0,
            new_obj: function(name){
                inp = $('.param input');
                this.name[this.name.length] = name;
                tmp = new Array();
                for(i=0; i<inp.length;i++){
                    tmp[i] = inp[i].checked;//tmp;
                }
                this.value[this.value.length] = tmp;
                console.log(this.value);
            },
            change_name: function(name){
                //console.log(name);
                ind = this.name.indexOf(name);
                val = this.value[ind];
                inp = $('.param input');
                for(i=0; i<inp.length;i++){
                    inp[i].checked = val[i];
                }
            },
            change_val: function(name){
                ind = this.name.indexOf(name);
                if(ind != -1){
                    inp = $('.param input');
                    tmp = this.value[ind];
                    for(i=0; i<inp.length;i++){
                        //vals = inp[i].checked;
                        tmp[i] = inp[i].checked;
                    }
                    this.value[ind] = tmp;
                }
            },
            saved: function(type){
                form = $('#myForm');
                sav = form[0].dataset.save;
                url = $('#ajax').val();
                if(sav == "no"){
                    gr_name = $('#field_name').val();
                    gr_alias = $('#field_alias').val();

                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            gr_name: gr_name,
                            gr_alias: gr_alias,
                            name: this.name,
                            val: this.value,
                            type: type
                        },
                        beforeSend: function(){
                            //tool_bar.showOverlayWait($('body'));
                            //timeout = setTimeout(function(){
                            //    tool_bar.showOverlayWait($('body'));
                            //},200);
                        },
                        comlete: function(){
                            //clearTimeout(timeout);
                            //tool_bar.hideOverlayWait();
                        },
                        success: function(data){
                            //clearTimeout(timeout);
                            //tool_bar.hideOverlayWait();
                            if(data.success){
                                if(data.msg){
                                    $('.Msg #msg-icon').html('<i class="fa '+data.msg.ico+'"></i>');
                                    $('.Msg .title').addClass('err').html(data.msg.title);
                                    $('.Msg .text').addClass('err').html(data.msg.text);
                                    $('.Msg').show().addClass('show');
                                    var m_timer = setTimeout(function(){
                                        $('.Msg').removeClass('show').hide();
                                    }, data.msg.time);
                                    if(data.redirect){
                                        var r_timer = setTimeout(function(){
                                            window.location.href = data.redirect;
                                        }, data.msg.time+1000);
                                    }
                                }else{
                                    if(data.redirect){
                                        window.location.href = data.redirect;
                                    }
                                }
                            }
                        },
                        error: function(data){
                            //clearTimeout(timeout);
                            //tool_bar.hideOverlayWait();
                            alert('Error' + data);
                        }
                    });
                }
            }
        }
    $('#field_<?php echo $obj->alias; ?>').on('change', function(){
        var name = $('#field_<?php echo $obj->alias; ?>')[0].value;
        group.change_name(name);
    })
    $('.sell-add').on('click', function(){
        $('.becks').show();
    });
    $('.sell-minus').on('click', function(){
        var sell = $('select#field_<?php echo $obj->alias; ?>')[0];
        sell.options[sell.selectedIndex] = null;
        //alert('елемент удален из списка');
    });
    $('.sell-sort').on('click', function(){
        var sell = $('select#field_<?php echo $obj->alias; ?>')[0];
        var Val = $('input[name="valSell"]')[0];
        var opt = sell.options;
        var opt_arr = new Array();
        for(var i=0; i<opt.length;i++){
            opt_arr[i]=opt[i].value;
        }
        opt_arr.sort();
        Val.value = opt_arr.join();
        sell.options.length = 0;
        for(var i=0; i<opt_arr.length;i++){
            sell.options[i] = new Option(opt_arr[i], opt_arr[i]);
        }
    });
    function btn_add_sell(){
        var sell = $('select#field_<?php echo $obj->alias; ?>')[0];
        var name = $('input[name="selAddname"]')[0];
        sell.options[sell.options.length] = new Option(name.value, name.value);
        group.new_obj(name.value);
        name.value ='';
        new_add();
        $('.beck').hide();
    }
    function btn_add_sell_new(){
        var sell = $('select#field_<?php echo $obj->alias; ?>')[0];
        var name = $('input[name="selAddname"]')[0];
        sell.options[sell.options.length] = new Option(name.value, name.value);
        group.new_obj(name.value);
        name.value ='';
        new_add();
    }
    function new_add(){
        var sell = $('select#field_<?php echo $obj->alias; ?>')[0].options;
        var Val = $('input[name="valSell"]')[0];
        var param = new Array();
        for(i=0;i<sell.length;i++){
            param[param.length] = sell[i].value;
        }
        param.sort();
        Val.value = param.join();
    }
    function new_press (e) {
        if(e.keyCode == 13){
            btn_add_sell_new();
        }
        if(e.keyCode == 27){
            $('.becks').hide();
        }
    }
</script>

