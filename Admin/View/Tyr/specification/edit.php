<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 12.12.2015
 * Time: 13:34
 */
?>

<form id="myForm" class="rowSection validat" method="post" action="/admin/tyr/specifications/edit" enctype="multipart/form-data" data-save = "yes">
    <input id="field_id" class="form-control valid" type="hidden" name="id" value="<?php echo $obj->id; ?>"/>
    <input type="hidden" id="ajax" value="/admin/tyr/specif/editAjax" >
    <!--div class="form-actions" style="display: none;">
        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
    </div-->
    <div class="form-group">
        <label class="control-label bs-tooltip" for="field_name" data-title="Название обязательно для заполнения">Название</label>
        <div class="">
            <input id="field_name" class="form-control valid" type="text" name="name" value="<?php echo $obj->name; ?>"/>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label bs-tooltip" for="field_name" data-title="Алиас обязательно для заполнения">Алиас</label>
        <div class="">
            <input id="field_alias" class="form-control valid" type="text" name="alias" value="<?php echo $obj->alias; ?>"/>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label bs-tooltip" for="field_group" data-title="для продолжения выберите тип характеристики">Тип</label>
        <div class="">
            <select id="field_group" class="form-control valid" name="group">
                <?php foreach($group as $itm): ?>

                    <option value="<?php echo $itm->alias; ?>" <?php echo $obj->group == $itm->alias ? 'selected' : NULL; ?>><?php echo $itm->name; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label bs-tooltip" for="field_type" data-title="для продолжения выберите вид характеристики">Вид</label>
        <div class="">
            <select id="field_type" class="form-control valid" name="type">
                <?php foreach($pole AS $v): ?>
                    <option value="<?php echo $v['alias']; ?>" <?php echo $obj->type == $v['alias'] ? 'selected' : NULL; ?>><?php echo $v['name']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>

    <div id="specif" class="form-group" style="min-height: 200px;">
        <?php echo \Core\View::tpl(array('obj' => $obj), 'Widgets/Control/Control'); ?>
    </div>


</form>
<script>
    $(function(){

        $('#field_name').on('change', function(){
            $('#field_name').removeClass('red');
        });
        $('#field_type').on('change', function(){
            if(form_valid())
                $('#myForm').submit();
        });
        $('#field_group').on('change', function(){
            if(form_valid()){
                var sel = $('#field_group')[0].value;
                var name = $('input[name="name"]')[0].value;
                if(sel == 'group'){
                    if(window.location.pathname != '/admin/tyr/specifications/group')
                        window.location = '/admin/tyr/specifications/group?link=edit&name='+name;
                }
                if(sel == 'base'){
                    if(window.location.pathname != '/admin/tyr/specifications/edit')
                        window.location = '/admin/tyr/specifications/edit';
                }
            }
        });
        $('.closed').on('click', function(){
            $('.beck').hide();
        })
        function form_valid(){
            if($('#field_name').val != ''){
                return true;
            }
            $('#field_name').addClass('red');
            return false;
        }

    });
</script>
