<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 20.12.2015
 * Time: 11:48
 */
?>
<form id="myForm" class="rowSection validat" method="post" action="/admin/tyr/add" enctype="multipart/form-data" data-save = "no">
    <input id="field_id" class="form-control valid" type="hidden" name="id" value="<?php echo $obj->id; ?>"/> <?php // id тура ?>
    <input id="field_ret" class="form-control valid" type="hidden" name="ret" value="<?php echo $obj->link; ?>"/><?php // урл куда вернемся если отмена ?>
    <input type="hidden" id="ajax" value="/admin/tyr/addAjax" ><?php // урл куда стучим если Ajax ?>

    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-cubes"></i>
                    Основные данные
                </div>
            </div>

            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-actions" style="display: none;">
                        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Опубликовано</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="status" value="0" type="radio" <?php echo (!$obj->status AND $obj) ? 'checked' : ''; ?>>
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="status" value="1" type="radio" <?php echo ($obj->status OR !$obj) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="f_name">Название</label>
                        <div class="">
                            <input class="form-control translitSource valid" id="f_name" name="FORM[name]" type="text" value="<?php echo $obj->name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="f_alias">
                            Алиас
                            <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>Алиас (англ. alias - псевдоним)</b><br>Алиасы используются для короткого именования страниц. <br>Предположим, имеется страница с псевдонимом «<b>about</b>». Тогда для вывода этой страницы можно использовать или полную форму: <br><b>http://domain/?go=frontend&page=about</b><br>или сокращенную: <br><b>http://domain/about.html</b>"></i>
                        </label>
                        <div class="">
                            <div class="input-group">
                                <input class="form-control translitConteiner valid" id="f_alias" name="FORM[alias]" type="text" value="<?php echo $obj->alias; ?>" />
                                <span class="input-group-btn">
                                    <button class="btn translitAction" type="button">Заполнить автоматически</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Акция</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="sale" value="0" type="radio" <?php echo !$obj->sale ? 'checked' : ''; ?>>
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="sale" value="1" type="radio" <?php echo $obj->sale ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group costField">
                        <label class="control-label" for="f_cost">Цена</label>
                        <div class="">
                            <input class="form-control valid" id="f_cost" name="FORM[cost]" type="number" step="10" value="<?php echo $obj->cost; ?>" />
                        </div>
                    </div>
                    <div class="form-group hiddenCostField" <?php echo !$obj->sale ? 'style="display:none;"' : ''; ?>>
                        <label class="control-label" for="f_old_cost">Старая цена</label>
                        <div class="">
                            <input class="form-control" id="f_old_cost" name="FORM[cost_old]" type="number" step="10" value="<?php echo $obj->cost_old; ?>" />
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <div class="col-md-5">
        <div class="widget">
            <div class="widgetHeader myWidgetHeader" style="position: relative;">
                <div class="widgetTitle">
                    <i class="fa fa-file-image-o"></i>
                    Картинки
                </div>
                <div class="widgetBtn">
                    <i title="Открыть сохраненные на сервере" class="fa fa-folder-open-o bs-tooltip btn-open-serv"></i>
                    <i title="Открыть для загрузки" class="fa fa-download bs-tooltip btn-open-user"></i>
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-group img-block">
                    <div class="img-manage" style="display: none;">
                        <div class="img-content">
                            <div class="window-head">
                                <div class="manage-title">Менеджер изображений</div>
                                <button type="button" class="manage-close">×</button>
                                <div class="manage-dragh"></div>
                            </div>
                            <div class="manage-container-body">
                                <div class="panel">
                                    <div class="panels-title">

                                    </div>

                                </div>
                                <div class="content">
                                    <div class="panels-title">

                                    </div>


                                </div>
                            </div>
                            <div class="manage-foot" hidefocus="1" tabindex="-1" role="group">
                                <div class="manage-foot-body">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="widget">
            <div class="widgetHeader myWidgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-cogs"></i>
                    Характеристики
                </div>
            </div>
            <div class="widgetContent">
                <?php foreach($group as $obj): ?>
                <?php echo Core\View::tpl( array('group' => $obj),'Tyr/tyr/form-group');  ?>
                <?php endforeach; ?>
            </div>
            <?php if( $obj->id ): ?>
                <div class="pageInfo alert alert-info">
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Опубликован</strong></div>
                        <div class="col-md-6"><?php echo $obj->status == 1 ? 'Да' : 'Нет'; ?></div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Дата создания товара</strong></div>
                        <div class="col-md-6"><?php echo $obj->created_at ? date('d.m.Y H:i:s', $obj->created_at) : '---'; ?></div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Дата последнего редактирования</strong></div>
                        <div class="col-md-6"><?php echo $obj->updated_at ? date('d.m.Y H:i:s', $obj->updated_at) : '---'; ?></div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Количество просмотров товара</strong></div>
                        <div class="col-md-6"><?php echo $obj->views; ?></div>
                    </div>
                </div>
            <?php endif ?>

        </div>
    </div>
</form>
<script>
    var img_obj = {
        block_draw:0,
        tree:0,

        init: function(){
            this.getFold('base');
            this.draw();
        },
        getFold: function(fold){
            $.ajax({
                url: '/backend/manager/getfolder',
                type: 'POST',
                data: {
                    folder: fold
                },
                success: function(data){
                    console.log(data);
                }
            });
        },
        draw: function(){
            htm = "";
            // $('.img-block').html(htm);
        },
    }
    img_obj.init();

    $('.btn-open-serv').on('click', function(){
        $('.img-manage').show();
    });
    $('.manage-close').on('click', function(){
        $('.img-manage').hide();
    });

    $('.btn-open-user').on('click', function(){

    });

</script>
<style>
    .widgetBtn{
        position: absolute;
        right: 10px;
        top: 0px;
        font-size: larger;
        padding: 5px;
        line-height: 30px;
    }
    .widgetBtn > i{
        cursor: pointer;
        padding: 5px;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.6);
        border-radius: 5px;
    }
    .widgetBtn > i:hover {background: beige;}

    .img-manage{
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.6);
        z-index: 999999;
    }
    .img-block .img-content {
        position: absolute;
        display: block;
        left: 15%;
        top: 25%;
        width: 70%;
        height: 50%;
        box-shadow: 0px 0px 15px black;
        background: rgba(0, 0, 0, 0.8);
    }
    .img-manage .window-head {
        padding: 5px 15px;
        border-bottom: 1px solid #111A21;
        position: relative;
        background: #1F2E3C;
    }
        .window-head .manage-title{
            line-height: 20px;
            font-size: 15px;
            font-weight: bold;
            text-rendering: optimizelegibility;
            padding-right: 10px;
            color: #E2E1E1;
        }
        .window-head button.manage-close {
            position: absolute;
            right: 15px;
            top: 9px;
            font-size: 20px;
            font-weight: bold;
            line-height: 20px;
            color: #E2E1E1;
            cursor: pointer;
            height: 20px;
            overflow: hidden;
            background: transparent;
            padding: 0;
            border: 0;
        }
        .window-head .manage-dragh {
            position: absolute;
            top: 0;
            left: 0;
            cursor: move;
            width: 90%;
            height: 100%;
        }

    .manage-container-body {
        position: relative;
        width: 100%;
        height: 270px;
    }
    .img-block .panel {
        display: block;
        border: 2px solid #484848;
        height: 100%;
        width: 180px;
        background: rgba(255, 255, 255, 0.8);
        box-shadow: 0px 0px 5px rgba(255, 255, 255, 0.69);
    }
    .img-block .content {
        display: block;
        position: absolute;
        top: 0px;
        left: 190px;
        width: 415px;
        height: 100%;
        border: 2px solid #484848;
        background: rgba(228, 227, 227, 0.67);
        box-shadow: 0px 0px 5px rgba(255, 255, 255, 0.69);
    }
    .manage-foot {
        display: block;
        margin-top: 7px;
        width: 99.5%;
        height: 20px;
    }
    .manage-foot-body {
        position: relative;
        width: 100%;
        height: 100%;
        border-radius: 4px;
        border: 2px solid #484848;
    }
</style>
