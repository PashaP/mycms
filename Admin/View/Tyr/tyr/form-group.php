<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 20.12.2015
 * Time: 12:18
 */
?>
<div class="form-group">
    <label class="control-label" for="f_brand"><?php echo $group->name; ?></label>
    <div class="">
        <div class="controls">
            <select class="form-control" id="f_brand" name="FORM[<?php echo $group->alias; ?>]" id="<?php echo $group->alias; ?>">
                <option value="0">Нет</option>
                <?php foreach( $group->sell as $obj ): ?>
                    <option value="<?php echo $obj; ?>" <?php echo $obj == $obj->brand_id ? 'selected' : ''; ?>><?php echo $obj; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
</div>
