<?php defined('SYSPATH') or die('No direct script access.');

class Model_Payments extends Model {
	
	### FOR CRON BALANCES START
	public function get_balances_from_db() {
		$result = DB::query(Database::SELECT, 'SELECT `balances`.`balance`,`balances`.`payment_system` AS `system`, `balances`.`currency` AS `cur` FROM `balances`')->as_object()->execute();
		$arr	= Array();
		foreach($result AS $obj) {
			$arr[$obj->system][$obj->cur] = number_format($obj->balance, 2, '.', '');
		}
		return $arr;
	}
	
	// TODO
	public function get_balance_from_db($api_alias, $currency = 'UAH') {
		$result = DB::query(Database::SELECT, 'SELECT `balances`.`balance` FROM `balances` WHERE `balances`.`payment_system` = "'.$api_alias.'" AND `balances`.`currency` = "'.strtoupper($currency).'"')->as_object()->execute();
		if ($result) {
			return $result[0]->balance;
		}
		return '0.00';
	}
	
	public function get_all_balances() {
		$balances = array();
		$balances['pb24']			= $this->pb24Balances();
		$balances['okpay'] 			= $this->okpayAllBalances();
		$balances['qiwi']		 	= QIWI::factory()->getBalances();
		$balances['pm'] 			= $this->pmBalances();
		$balances['egopay'] 		= $this->egopayAllBalances();
		$balances['yandex']['RUB']	= $this->yandexBalance();
		$balances['bitcoin']['BTC']	= $this->bitcoinBalance();
		$balances['btce']['USD']	= $this->btceBalance();
		$balances 					= array_merge($balances, $this->wmAllBalances());
		return $balances;
	}
	
	public static function wmAllBalances() {
		$wm      = Webmoney::factory();
		$result  = $wm->WMXI()->X9()->GetResponse(false)->purses;
		$config  = Kohana::$config->load('wallets');
		$wallets = $wm->wallets();
		foreach($result->purse AS $obj) {
			if(in_array($obj->pursename, $wallets)) {
				if(strtoupper(trim($obj->desc)) == 'BTC') {
					$key  = 'BTC';
					$desc = 'WMX';
				} else if(strtoupper(trim($obj->desc)) == 'GOLD') {
					$key  = 'GOLD';
					$desc = 'WMG';
				} else {
					$key  = $wm->cur[strtolower(trim($obj->desc))];
					$desc = strtoupper(trim($obj->desc));
				}
				$arr[$desc][$key] = number_format((float) $obj->amount, 2, '.', '');
			}
		}
		return $arr;
	}
	
	public function egopayAllBalances() {
		$arr = array();
		$obj = EgoPay::factory()->get_balance();
		if($obj->status == 'ok') {
			unset($obj->status);
			if (!empty($obj) AND is_object($obj)) {
				foreach($obj AS $key => $value) {
					$arr[$key] = number_format((float) $value, 2, '.', '');
				}
			}
		}
		return $arr;
	}
	
	public static function okpayAllBalances() {
		$arr = array();
		$obj = Okpay::factory()->Payments()->get_balance();
		if (!empty($obj) AND is_object($obj) AND isset($obj->Balance)) {
			foreach ($obj->Balance AS $key => $value) {
				$arr[$value->Currency] = number_format((float) $value->Amount, 2, '.', '');
			}
		}
		return $arr;
    }
	### FOR CRON BALANCES END
	
    public function get_balance($purse, $currency = 'UAH') {
    	if (strtolower($purse) == 'bitcoin') {
			return $this->bitcoinBalance();
		}
		if (strtolower($purse) == 'btce') {
			return $this->btceBalance();
		}
		if (strtolower($purse) == 'egopay') {
			return $this->egopayBalance($currency);
		}
		if (strtolower($purse) == 'okpay') {
			return $this->okpayBalance($currency);
		}
		if (strtolower($purse) == 'qiwi') {
			return QIWI::factory()->getBalances($currency);
		}
		if (strtolower($purse) == 'pm' OR strtolower($purse) == 'perfectmoney') {
			return $this->pmBalance($currency);
		}
		if (strtolower($purse) == 'pb24' OR strtolower($purse) == 'privat24') {
			return $this->pb24Balance();
		}
		if (strtolower($purse) == 'yandex' OR strtolower($purse) == 'yam') {
			return $this->yandexBalance();
		}
		if (substr(strtolower($purse), 0, -1) == 'wm') {
			return $this->wmBalance($purse);
		}
	}
	
	public function get_balances($systems = '', $currencies) {
		if(!is_array($currencies)) { $currencies = Array($currencies); }
		$balances = array();
		$systems  = explode(',', $systems);
		$systems  = array_map('trim', $systems);
		if (in_array('egopay', $systems)) {
			$balances['egopay'] 		= $this->egopayBalances($currencies);
		}
		if (in_array('okpay', $systems)) {
			$balances['okpay'] 			= $this->okpayBalances($currencies);
		}
		if (in_array('qiwi', $systems)) {
			$balances['qiwi'] 			= QIWI::factory()->getBalances();
		}
		if (in_array('pm', $systems) OR in_array('perfectmoney', $systems)) {
			$balances['pm'] 			= $this->pmBalances();
		}
		if (in_array('pb24', $systems) OR in_array('privat24', $systems)) {
			$balances['pb24']			= $this->pb24Balances();
		}
		if (in_array('yandex', $systems) OR in_array('yam', $systems)) {
			$balances['yandex']['RUB']	= $this->yandexBalance();
		}
		if (in_array('bitcoin', $systems)) {
			$balances['bitcoin']['BTC']	= $this->bitcoinBalance();
		}
		if (in_array('btce', $systems)) {
			$balances['btce']['USD']	= $this->btceBalance();
		}
		if (in_array('wmr', $systems) OR in_array('wmz', $systems) OR in_array('wmb', $systems) OR in_array('wmu', $systems) OR in_array('wme', $systems)) {
			$webmoney = $this->wmBalances($currencies);
			$balances = array_merge($balances, $webmoney);
		}
		return $balances;
	}
	
	public function yandexBalance() {
		$res	= ORM::factory('UserYmtoken')->where('purse', '=', Kohana::$config->load('ym')->get('purse'))->find();
		if(!$res->loaded()) { return '0.00'; }
		$res 	= YandexMoney::factory()->accountInfo($res->purse.'.'.$res->token)->getBalance();
		if(!$res) { return '0.00'; }
		return (float) $res;
	}
	
	public function pb24Balance() {
		return PrivatBank::factory()->balance();
	}
	
	public function pb24Balances() {
		return Array('UAH' => PrivatBank::factory()->balance());
	}
	
	public function wmBalance($currency) {
		$wm      = Webmoney::factory();
		$result  = $wm->WMXI()->X9()->GetResponse(false)->purses;
		$config  = Kohana::$config->load('wallets');
		$wallets = $wm->wallets();
		foreach($result->purse AS $obj) {
			if(in_array($obj->pursename, $wallets)) {
				if($currency == strtolower(trim($obj->desc))) {
					$arr = (array) $obj->amount;
					return number_format((float) $arr[0], 2, '.', '');
				}
			}
		}
		return '0.00';
	}
	
	public static function wmBalances($currencies) {
		$wm      = Webmoney::factory();
		$result  = $wm->WMXI()->X9()->GetResponse(false)->purses;
		$config  = Kohana::$config->load('wallets');
		$wallets = $wm->wallets();
		foreach($result->purse AS $obj) {
			if(in_array($obj->pursename, $wallets)) {
				$cur = strtolower(trim($obj->desc)) == 'btc' ? 'wmx' : strtolower(trim($obj->desc));
				$key = $wm->cur[$cur];
				if (in_array($key, $currencies)) {
					$arr[strtolower(trim($obj->desc))][$key] = number_format((float) $obj->amount, 2, '.', '');
				}
			}
		}
		return $arr;
	}
    
    public function okpayBalance($currency) {
		$obj = Okpay::factory()->Payments()->get_balance();
		if (!empty($obj) AND is_object($obj) AND isset($obj->Balance)) {
			foreach ($obj->Balance AS $key => $value) {
				if (strtoupper($value->Currency) == strtoupper($currency)) {
					return number_format((float) $value->Amount, 2, '.', '');
				}
			}
		}
		return '0.00';
    }
	
	public static function okpayBalances($currencies) {
		$arr = array();
		$obj = Okpay::factory()->Payments()->get_balance();
		if (!empty($obj) AND is_object($obj) AND isset($obj->Balance)) {
			foreach ($obj->Balance AS $key => $value) {
				if (!empty($value) AND in_array($value->Currency, $currencies)) {
					$arr[$value->Currency] = number_format((float) $value->Amount, 2, '.', '');
				}
			}
		}
		return $arr;
    }
    
    public function egopayBalance($currency) {
		$obj = EgoPay::factory()->get_balance();
		if (!empty($obj) AND is_object($obj)) {
			foreach($obj AS $key => $value) {
				if (strtolower($key) == strtolower($currency)) {
					return number_format((float) $value, 2, '.', '');
				}
			}
		}
		return '0.00';
    }

	public function egopayBalances($currencies) {
		$arr = array();
		$obj = EgoPay::factory()->get_balance();
		if (!empty($obj) AND is_object($obj)) {
			foreach($obj AS $key => $value) {
				if (in_array($key, $currencies)) {
					$arr[$key] = number_format((float) $value, 2, '.', '');
				}
			}
		}
		return $arr;
    }
    
    public function pmBalance($currency) {
		$balances 	= PM::factory()->get_balance();
		if (!empty($balances)) {
			foreach($balances AS $key => $value) {
				if(strtolower($currency) == 'usd' AND strtolower($key[0]) == 'u') {
					return number_format((float) $value, 2, '.', '');
				} else if(strtolower($currency) == 'eur' AND strtolower($key[0]) == 'e') {
					return number_format((float) $value, 2, '.', '');
				} else if(strtolower($currency) == 'gold' AND strtolower($key[0]) == 'g') {
					return number_format((float) $value, 2, '.', '');
				}
			}
		}		
		return '0.00';
    }
	
	public static function pmBalances() {
		$arr 		= array();
		$balances 	= PM::factory()->get_balance();
		if (!empty($balances)) {
			foreach($balances AS $key => $value) {
				if($key[0] == 'U') {
					$arr['USD']  = number_format((float) $value, 2, '.', '');
				} else if($key[0] == 'E') {
					$arr['EUR']  = number_format((float) $value, 2, '.', '');
				} else if($key[0] == 'G') {
					$arr['GOLD'] = number_format((float) $value, 2, '.', '');
				}
			}
		}
		return $arr;
    }

    public function bitcoinBalance() {
		$Blockchain = Blockchain::factory();
		$Blockchain->Wallet->credentials($Blockchain->get_identifier(), $Blockchain->get_password_1(), $Blockchain->get_password_2());
		$balance = $Blockchain->Wallet->getBalance();
		return number_format($balance, 2, '.', '');
	}

	public function btceBalance() {
		$BTCE = BTCE::factory();
		$info = $BTCE->getInfo();
		if ( Arr::get( $info, 'success', 0 ) != 1 ) {
			return 0.00;
		}
		$return = Arr::get( $info, 'return', array() );
		$funds = Arr::get( $return, 'funds', array() );
		$usd = Arr::get( $funds, 'usd', 0 );
		return number_format($usd, 2, '.', ''); 
	}
	
	public function get_hand_balalnces() {
		$balances=array();
		$systems=Model::factory('PaymentSystem')->get_hand_systems();
		foreach ($systems as $syst) {
			$balances[$syst->api_alias]=array();
			$currencies=Model::factory('CurrencyFrom')->get_all_currencies_for_system($syst->id);
			foreach ($currencies as $curr) {                
				$balances[$syst->api_alias][$curr->alias]=$curr->reserve;
			}
			
			
		}
		return $balances;
		
	}
    
}