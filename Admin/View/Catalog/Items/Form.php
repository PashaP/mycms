<form id="myForm" class="rowSection validat" method="post" action="" enctype="multipart/form-data">
    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Основные данные
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-actions" style="display: none;">
                        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Опубликовано</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="status" value="0" type="radio" <?php echo (!$obj->status AND $obj) ? 'checked' : ''; ?>>
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="status" value="1" type="radio" <?php echo ($obj->status OR !$obj) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="f_artikul">Артикул</label>
                        <div class="">
                            <input class="form-control" id="f_artikul" name="FORM[artikul]" type="text" value="<?php echo $obj->artikul; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="f_name">Название</label>
                        <div class="">
                            <input class="form-control translitSource valid" id="f_name" name="FORM[name]" type="text" value="<?php echo $obj->name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="f_alias">
                            Алиас
                            <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>Алиас (англ. alias - псевдоним)</b><br>Алиасы используются для короткого именования страниц. <br>Предположим, имеется страница с псевдонимом «<b>about</b>». Тогда для вывода этой страницы можно использовать или полную форму: <br><b>http://domain/?go=frontend&page=about</b><br>или сокращенную: <br><b>http://domain/about.html</b>"></i>
                        </label>
                        <div class="">
                            <div class="input-group">
                                <input class="form-control translitConteiner valid" id="f_alias" name="FORM[alias]" type="text" value="<?php echo $obj->alias; ?>" />
                                <span class="input-group-btn">
                                    <button class="btn translitAction" type="button">Заполнить автоматически</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="f_group">Группа</label>
                        <div class="">
                            <div class="controls">
                                <select class="form-control valid" id="f_group" name="FORM[parent_id]" id="parent_id">
                                    <option value="">Не выбрано</option>
                                    <?php echo $tree; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Акция</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="sale" value="0" type="radio" <?php echo !$obj->sale ? 'checked' : ''; ?>>                            
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="sale" value="1" type="radio" <?php echo $obj->sale ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group costField">
                        <label class="control-label" for="f_cost">Цена</label>
                        <div class="">
                            <input class="form-control valid" id="f_cost" name="FORM[cost]" type="number" step="10" value="<?php echo $obj->cost; ?>" />
                        </div>
                    </div>
                    <div class="form-group hiddenCostField" <?php echo !$obj->sale ? 'style="display:none;"' : ''; ?>>
                        <label class="control-label" for="f_old_cost">Старая цена</label>
                        <div class="">
                            <input class="form-control" id="f_old_cost" name="FORM[cost_old]" type="number" step="10" value="<?php echo $obj->cost_old; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Новинка</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="new" value="0" type="radio" <?php echo !$obj->new ? 'checked' : ''; ?>>                            
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="new" value="1" type="radio" <?php echo $obj->new ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Популярный товар</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="top" value="0" type="radio" <?php echo !$obj->top ? 'checked' : ''; ?>>                            
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="top" value="1" type="radio" <?php echo $obj->top ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Наличие</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="available" value="0" type="radio" <?php echo !$obj->available ? 'checked' : ''; ?>>                            
                                Нет в наличии
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="available" value="1" type="radio" <?php echo $obj->available == 1 ? 'checked' : ''; ?>>
                                Есть в наличии
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="available" value="2" type="radio" <?php echo $obj->available == 2 ? 'checked' : ''; ?>>
                                Под заказ
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Пол</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="sex" value="0" type="radio" <?php echo !$obj->sex ? 'checked' : ''; ?>>                            
                                Мужчинам
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="sex" value="1" type="radio" <?php echo $obj->sex == 1 ? 'checked' : ''; ?>>
                                Женщинам
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="sex" value="2" type="radio" <?php echo $obj->sex == 2 ? 'checked' : ''; ?>>
                                Унисекс
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="widget">
            <div class="widgetHeader myWidgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Характеристики
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-group">
                    <label class="control-label" for="f_brand">Бренд</label>
                    <div class="">
                        <div class="controls">
                            <select class="form-control" id="f_brand" name="FORM[brand_id]" id="brand_id">
                                <option value="0">Нет</option>
                                <?php foreach( $brands as $brand ): ?>
                                    <option value="<?php echo $brand->id; ?>" <?php echo $brand->id == $obj->brand_id ? 'selected' : ''; ?>><?php echo $brand->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="f_model">Модель</label>
                    <div class="">
                        <div class="controls">
                            <select class="form-control" id="f_model" name="FORM[model_id]" id="model_id">
                                <option value="0">Нет</option>
                                <?php foreach( $models as $model ): ?>
                                    <option value="<?php echo $model->id; ?>" <?php echo $model->id == $obj->model_id ? 'selected' : ''; ?>><?php echo $model->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-vertical row-border" id="specGroup">
                    <?php foreach ($specifications as $spec): ?>
                        <?php if (count($specValues[$spec->id])): ?>
                            <div class="form-group">
                                <label class="control-label"><?php echo $spec->name; ?></label>
                                <div class="">
                                    <div class="controls">
                                        <?php if ($spec->type_id == 3): ?>
                                            <select class="form-control" name="SPEC[<?php echo $spec->id; ?>][]" multiple="10" style="height:150px;">
                                                <?php foreach( $specValues[$spec->id] as $value ): ?>
                                                    <option value="<?php echo $value->id; ?>" <?php echo (isset($specArray[$spec->id]) AND in_array($value->id, $specArray[$spec->id])) ? 'selected' : ''; ?>><?php echo $value->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        <?php endif ?>
                                        <?php if ($spec->type_id == 2 OR $spec->type_id == 1): ?>
                                            <select class="form-control" name="SPEC[<?php echo $spec->id; ?>]">
                                                <option value="0">Нет</option>
                                                <?php foreach( $specValues[$spec->id] as $value ): ?>
                                                    <option value="<?php echo $value->id; ?>" <?php echo (isset($specArray[$spec->id]) AND $specArray[$spec->id] == $value->id) ? 'selected' : ''; ?>><?php echo $value->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                    <?php endforeach ?>
                </div>
            </div>
            <?php if( $obj->id ): ?>
                <div class="pageInfo alert alert-info">
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Опубликован</strong></div>
                        <div class="col-md-6"><?php echo $obj->status == 1 ? 'Да' : 'Нет'; ?></div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Дата создания товара</strong></div>
                        <div class="col-md-6"><?php echo $obj->created_at ? date('d.m.Y H:i:s', $obj->created_at) : '---'; ?></div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Дата последнего редактирования</strong></div>     
                        <div class="col-md-6"><?php echo $obj->updated_at ? date('d.m.Y H:i:s', $obj->updated_at) : '---'; ?></div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Количество просмотров товара</strong></div>
                        <div class="col-md-6"><?php echo $obj->views; ?></div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</form>

<?php echo $uploader; ?>
<?php echo $related; ?>

<script>
    $(function(){
        $('input[name="sale"]').on('change', function(){
            var val = parseInt( $(this).val() );
            if( val ) {
                var cost = $('input[name="FORM[cost]"]').val();
                var cost_old = $('input[name="FORM[cost_old]"]').val();
                $('input[name="FORM[cost]"]').val(cost_old);
                $('input[name="FORM[cost_old]"]').val(cost);
                $('.hiddenCostField').show();
            } else {
                var cost = $('input[name="FORM[cost]"]').val();
                var cost_old = $('input[name="FORM[cost_old]"]').val();
                $('input[name="FORM[cost]"]').val(cost_old);
                $('input[name="FORM[cost_old]"]').val(cost);
                $('.hiddenCostField').hide();
            }
        });

        $('#parent_id').on('change', function(){
            var catalog_tree_id = $(this).val();
            $.ajax({
                url: '/wezom/ajax/catalog/getSpecificationsByCatalogTreeID',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    catalog_tree_id: catalog_tree_id
                },
                success: function(data){
                    var html = '<option value="0">Нет</option>';
                    for(var i = 0; i < data.brands.length; i++) {
                        html += '<option value="' + data.brands[i].id + '">' + data.brands[i].name + '</option>';
                    }
                    $('#brand_id').html(html);
                    html = '';
                    for(var i = 0; i < data.specifications.length; i++) {
                        var spec = data.specifications[i];
                        if( data.specValues[spec.id].length ) {
                            var values = data.specValues[spec.id];
                            html += '<div class="form-group"><label class="control-label">'+spec.name+'</label>';
                            html += '<div class=""><div class="controls">';
                            if( parseInt( spec.type_id ) == 3 ) {
                                html += '<select class="form-control" name="SPEC['+spec.id+'][]" multiple="10" style="height:150px;">';
                                for( var j = 0; j < values.length; j++ ) {
                                    var val = values[j];
                                    html += '<option value="'+val.id+'">'+val.name+'</option>';
                                }
                                html += '</select>';
                            }
                            if( parseInt( spec.type_id ) == 2 || parseInt( spec.type_id ) == 1 ) {
                                html += '<select class="form-control" name="SPEC['+spec.id+']">';
                                html +='<option value="0">Нет</option>';
                                for( var j = 0; j < values.length; j++ ) {
                                    var val = values[j];
                                    html += '<option value="'+val.id+'">'+val.name+'</option>';
                                }
                                html += '</select>';
                            }
                            html += '</div></div></div>';
                        }
                    }
                    $('#specGroup').html(html);
                }
            });
        });

        $('#brand_id').on('change', function(){
            var brand_id = $(this).val();
            $.ajax({
                url: '/wezom/ajax/catalog/getModelsByBrandID',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    brand_id: brand_id
                },
                success: function(data){
                    var html = '<option value="0">Нет</option>';
                    for(var i = 0; i < data.options.length; i++) {
                        html += '<option value="' + data.options[i].id + '">' + data.options[i].name + '</option>';
                    }
                    $('#model_id').html(html);
                }
            });
        });
    });
</script>