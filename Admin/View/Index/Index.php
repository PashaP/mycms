<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 19.11.2015
 * Time: 7:52
 */
?>
<div class="rowSection">
<div class="col-md-12">
    <div class="widget">
        <div class="widget">
            <div class="widgetContent">
                <table class="table table-striped table-hover checkbox-wrap ">
                    <thead>
                    <tr>
                        <th>Название Модуля</th>
                        <th>Состояние</th>
                    </tr>
                    </thead>
                    <tbody><?php //var_dump($result); ?>
                    <?php foreach ( $result as $obj ): ?><?php //var_dump($obj); ?>
                        <tr data-id="<?php echo $obj->id; ?>">
                            <td><a href="/admin/<?php echo Core\Route::controller(); ?>/edit/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a></td>
                            <td width="45" valign="top" class="icon-column status-column">
                                <?php if($obj->write){ ?>
                                <?php echo Core\View::widget(array( 'status' => $obj->active, 'id' => $obj->id,'toltip' => array('yes' => '<b>Снять с публикации</b><br>Опубликована' ,'no' => '<b>Опубликовать новость</b><br>Не опубликовано') ), 'StatusList'); ?>
                                <?php }else{ ?>
                                    <div class="tip liTipLink" data-title="Системный компонент отключить нельзя!!!">
                                        <i class="fa fa-cogs"></i>
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<span id="parameters" data-table="<?php echo $table; ?>"  data-pole="active"></span>