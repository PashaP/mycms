<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 21.12.2015
 * Time: 20:58
 */

namespace Admin\Module\Tyr\Controllers;

use Core\Arr;
use Core\Route;
use Core\View;
use Core\Msg;
use Core\QB\DB;
use Core\Text;
use Admin\Module\Tyr\Models\TyrMod as Model;
use Admin\Module\Ajax  as Aj;

class FileAjax extends \Admin\Module\Base
{
    public function before()
    {
        $this->html = Arr::init_object();
        $this->post = $_POST;
    }

    public function getFolderAction()
    {
        $group_name = Arr::get($_POST, 'gr_name');
        die('file');
    }
}