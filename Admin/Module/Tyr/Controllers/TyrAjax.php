<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 12.12.2015
 * Time: 16:43
 */

namespace Admin\Module\Tyr\Controllers;

use Core\Arr;
use Core\Route;
use Core\View;
use Core\Msg;
use Core\QB\DB;
use Core\Text;
use Admin\Module\Tyr\Models\TyrMod as Model;
use Admin\Module\Ajax  as Aj;

class TyrAjax extends \Admin\Module\Base
{
    public function before()
    {

        $this->setBreadcrumbs('Главная', 'admin/');
        $this->_template = 'Main';
        $this->html = Arr::init_object();
        $this->post = $_POST;
    }
    public function specif_addAction()
    {
        $post = $this->post;
        $data =  (object)Arr::get_UnSerialize(Arr::get($_POST,'data'));
        $btn_type = Arr::get( $post, 'btn_type', 'no' );

        if($btn_type != 'no' and $btn_type != 'exit'){
            // Set default settings for some fields
            $data->alias = Text::translit($data->name);

            $result = DB::select()
                ->from('tyr_specifications')
                ->where('name','=',$obj->name)
                ->where('type','=',$obj->type)
                ->execute();

            if($result[0]){
                Msg::factory()->setMsg('err','icon-warning3','Ошибка', 'Данное имя уже используется, имя должно быть уникальным',2000,1);
                Aj::Aj_Error();
                die('error!!!');
            }else{
                if(!isset($data->valSell))
                    $data->valSell = '';
                $create_at = mktime ();
                $upd = DB::insert('tyr_specifications',array('name','alias','status','group','type','value','create_at','modif_at'))
                    ->values(array($data->name,$data->alias,1,$data->group,$data->type,$data->valSell,$create_at,$create_at))
                    ->execute();
            }
            //var_dump($upd);
            if($upd){
                Msg::factory()->setMsg('Ok','icon-warning3','Успешное Сохранение!!!','Данные были успешно сохранены!',3000,1);
            }else{
                Msg::factory()->setMsg('err','icon-warning3','Ошибка', 'При сохранении произошла ошибка, если данное сообщение повторяется постоянно обратитесь к программисту! ошибка',2000,1);
                Aj::Aj_Error();
                die('error!!!');
            }

        }else{
            if($btn_type != 'exit')
                die('error!!!');
        }
        switch ($btn_type) {
            case 'save-close':
                Aj::Aj_Success('/admin/tyr/specifications');
                break;
            case 'save-add':
                Aj::Aj_Success('/admin/tyr/specifications/add');
                break;
            case 'save':
                Aj::Aj_Success('/admin/tyr/specifications');
                break;
            case 'exit':
                Aj::Aj_Success('/admin/tyr/specifications');
                break;
        }
    }
    public function specif_editAction()
    {
        $post = $this->post;
        $data =  (object)Arr::get_UnSerialize(Arr::get($_POST,'data'));
        $btn_type = Arr::get( $post, 'btn_type', 'no' );

        if($btn_type != 'no' and $btn_type != 'exit'){
            // Set default settings for some fields
            $data->alias = Text::translit($data->name);
            if(!isset($data->valSell))
                $data->valSell = '';
            $modif_at = mktime ();

            $upd = DB::update('tyr_specifications')
                ->set(array('name'=>$data->name,'alias'=>$data->alias,'type'=>$data->type,
                     'group'=>$data->group,'value'=>$data->valSell,'modif_at'=>$modif_at))
                ->where('id','=',$data->id)
                ->execute();
            //var_dump($upd);
            if($upd == 1){
                Msg::factory()->setMsg('Ok','icon-warning3','Успешное Сохранение!!!','Данные были успешно обновлены!',3000,1);
            }else{
                Msg::factory()->setMsg('err','icon-warning3','Ошибка', 'При сохранении произошла ошибка, если данное сообщение повторяется постоянно обратитесь к программисту! ошибка',2000,1);
                Aj::Aj_Error();
                die('error!!!');
            }

        }else{
            if($btn_type != 'exit')
            die('error!!!');
        }
        switch ($btn_type) {
            case 'save-close':
                Aj::Aj_Success('/admin/tyr/specifications');
                break;
            case 'save-add':
                Aj::Aj_Success('/admin/tyr/specifications/add');
                break;
            case 'save':
                Aj::Aj_Success('');
                break;
            case 'exit':
                Aj::Aj_Success('/admin/tyr/specifications');
                break;
        }
    }
    public function groupAjaxAction(){
        $group_name = Arr::get( $_POST, 'gr_name' );
        $group_alias = Arr::get( $_POST, 'gr_alias' );
        $btn_type = Arr::get( $_POST, 'type', 'save-close' );
        $name = implode(',',Arr::get( $_POST, 'name' ));
        $val = json_encode( Arr::get( $_POST, 'val' ) );
        $ret = DB::select('id')
                ->from('tyr_specifications')
                ->where('alias','=',$group_alias)
                ->execute();

        if($ret[0] == null){
            $create_at = mktime ();
            $upd = DB::insert('tyr_specifications',array('name','alias','status','group','type','value','value_group','create_at','modif_at'))
                ->values(array($group_name,$group_alias,1,'group','select',$name,$val,$create_at,$create_at))
                ->execute();
        }else{
            $id = $ret[0]["id"];
            $modif_at = mktime ();
            $upd = DB::update('tyr_specifications')
                ->set(array('name'=>$group_name,'alias'=>$group_alias,'value'=>$name,
                    'value_group'=>$val ,'modif_at'=>$modif_at))
                ->where('id','=',$id)
                ->execute();
        }
        if($upd){
            Msg::factory()->setMsg('Ok','icon-warning3','Успешное Сохранение!!!','Данные были успешно сохранены!',3000,1);
        }else{
            Msg::factory()->setMsg('err','icon-warning3','Ошибка', 'При сохранении произошла ошибка, если данное сообщение повторяется постоянно обратитесь к программисту! ошибка',2000,1);
            Aj::Aj_Error();
            die('error!!!');
        }
        switch ($btn_type) {
            case 'save-close':
                Aj::Aj_Success('/admin/tyr/specifications');
                break;
            case 'save-add':
                Aj::Aj_Success('/admin/tyr/specifications/add');
                break;
            case 'save':
                Aj::Aj_Success('/admin/tyr/specifications');
                break;
            case 'exit':
                Aj::Aj_Success('/admin/tyr/specifications');
                break;
        }
    }
    public function send( $data ) {
        echo json_encode( $data );
        Msg::factory()->resetMsg();
        die;
    }
}