<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 24.11.2015
 * Time: 18:48
 */

namespace Admin\Module\Tyr\Controllers;

use Core\Arr;
use Core\Core;
use Core\Route;
use Core\View;
use Core\Msg;
use Core\QB\DB;
use Core\Text;
use Admin\Module\Tyr\Models\TyrMod as Model;
use Admin\Module\Ajax  as Aj;

class Tyr extends \Admin\Module\Base
{
    public function before()
    {
        $this->setBreadcrumbs('Главная', 'admin/');
        $this->_template = 'Main';
        $this->html = Arr::init_object();
        $this->post = $_POST;
    }
    // Tyr
    public function indexAction()
    {
        $this->_seo['h1'] = 'Туры';
        $this->_seo['title'] = 'Туры';
        $this->setBreadcrumbs('Туры', 'admin/'.Route::controller().'/index');
        $this->setModule('filter',array(),'Widgets/Toolbar/Filters/filter_tyr');
        $this->setModule('toolbar',array('link_add'=>'tyr/add',),'Widgets/Toolbar/NewDel');

        $this->html = Arr::init_object();
        $this->html->title = 'Туры';

        $tyr = Model::get_Tyrs();

        $this->html->content = View::tpl( array('module' => $this->_module,'result' => $tyr,),'Tyr/tyr/index');
        $this->_content = View::tpl( array( 'html' => $this->html, ),$this->_template );
        //View::tpl( array( 'html' => $this->html, ),$this->_template);
    }
    public function tyrAddAction(){
        $this->_seo['h1'] = 'Новый Тур';
        $this->_seo['title'] = 'Новый Тур';
        $this->setBreadcrumbs('Туры', 'admin/'.Route::controller().'/index');
        $this->setBreadcrumbs('Новый Тур', 'admin/'.Route::controller().'/index');
        $this->setModule('filter',array(),'Widgets/Toolbar/Filters/filter_tyr');
        $this->setModule('toolbar',array('link_add'=>'tyr/add',),'Widgets/Toolbar/NewDel');

        $this->html = Arr::init_object();
        $this->html->title = 'Новый Тур';

        $group = Model::getGroupAll();


        $this->html->content = View::tpl( array('module' => $this->_module,'group' => $group,),'Tyr/tyr/form');
        $this->_content = View::tpl( array( 'html' => $this->html, ),$this->_template );
    }

    //Specification
    public function specifAction()
    {
        $this->_seo['h1'] = 'Характеристики Туров';
        $this->_seo['title'] = 'Характеристики Туров';
        $this->setBreadcrumbs('Характеристики Туров', 'admin/'.Route::controller().'/index');
        $this->setModule('filter',array(),'Widgets/Toolbar/Filters/filter_tyr');
        $this->setModule('toolbar',array('link_add'=>'tyr/specifications/add',),'Widgets/Toolbar/NewDel');

        $this->html = Arr::init_object();
        $this->html->title = 'Характеристики Туров';

        $specif = Model::get_Specif_All();

        $this->html->content = View::tpl( array('module' => $this->_module,'result' => $specif, 'tablename' => 'tyr_specifications'),'Tyr/specification/index');
        $this->_content = View::tpl( array( 'html' => $this->html, ),$this->_template );
        //View::tpl( array( 'html' => $this->html, ),$this->_template);
    }
    public function specif_addAction()
    {
        $this->_seo['h1'] = 'Новая Характеристика';
        $this->_seo['title'] = 'Новая Характеристика';
        $this->setBreadcrumbs('Туры', '/admin/tyr/index');
        $this->setBreadcrumbs('Характеристики', 'admin/tyr/specifications');
        $this->setBreadcrumbs('Новая Характеристика', 'admin/'.Route::controller().'/index');
        $this->setModule('filter',array(),'Widgets/Toolbar/Filters/filter_tyr');

        $this->html = Arr::init_object();
        $this->html->title = 'Новая Характеристика';
        $obj = Arr::init_object();

        if ($_POST) {
            // Set default settings for some fields
            $obj->name = Arr::get( $_POST, 'name', 0 );
            $obj->type = Arr::get( $_POST, 'type', 0 );
            $obj->group = Arr::get( $_POST, 'group', 0 );
            $obj->id = rand(1,100000);
            $obj->title = Model::get_TypeTitle($obj->type);
            $ret = DB::select('value')
                ->from('tyr_specifications')
                ->where('name','=',$obj->name)
                ->where('type','=',$obj->type)
                ->execute();
            $obj->set_val = $ret[0]['value'];
        }
        $tyr = Model::get_Tyrs();
        $pole = Model::get_TypePole();
        $group = Model::get_Specif_group();
        $this->html->toolbar = View::tpl( array( 'html' => $this->html, ),'Widgets/Toolbar/EditSave');
        $this->html->content = View::tpl( array('module' => $this->_module,'result' => $tyr,
                                'pole' => $pole,'group'=>$group,'obj' => $obj),'Tyr/specification/add');
        $this->_content = View::tpl( array( 'html' => $this->html, ),$this->_template );
        //View::tpl( array( 'html' => $this->html, ),$this->_template);
    }
    public function specif_editAction()
{
    $post = $this->post;
    //var_dump($post);die;
    if(isset($post['id'])){
        $id = Arr::get( $post, 'id', 1 );
        $specif = Model::get_Specif($id);
        $specif->set_val = $specif->value;
        $specif->name = Arr::get( $_POST, 'name', $specif->name );
        $specif->alias = Arr::get( $_POST, 'name', $specif->alias );
        $specif->type = Arr::get( $_POST, 'type', $specif->type );
        $specif->group = Arr::get( $_POST, 'name', $specif->group );
    }

    $this->_seo['h1'] = 'Характеристика '.$specif->name;
    $this->_seo['title'] = 'Характеристика '.$specif->name;
    $this->setBreadcrumbs('Туры', '/admin/tyr/index');
    $this->setBreadcrumbs('Характеристики', 'admin/tyr/specifications');
    $this->setBreadcrumbs($specif->name, 'admin/tyr/specifications/edit');
    $this->setModule('filter',array(),'Widgets/Toolbar/Filters/filter_tyr');

    $this->html = Arr::init_object();
    $this->html->title = 'Характеристика '.$specif->name;

    $tyr = Model::get_Tyrs();
    $pole = Model::get_TypePole();
    $group = Model::get_Specif_group();
    $this->html->toolbar = View::tpl( array( 'html' => $this->html, ),'Widgets/Toolbar/EditFull');
    $this->html->content = View::tpl( array('module' => $this->_module,'result' => $tyr, 'pole' => $pole,'group'=>$group,
        'obj' => $specif),'Tyr/specification/edit');
    $this->_content = View::tpl( array( 'html' => $this->html, ),$this->_template );
}
    public function specif_groupAction()
    {
        $this->_seo['h1'] = 'Новая Группа Характеристик';
        $this->_seo['title'] = 'Новая Группа Характеристик';
        $this->setBreadcrumbs('Туры', '/admin/tyr/index');
        $this->setBreadcrumbs('Характеристики', 'admin/tyr/specifications');
        $this->setBreadcrumbs('Новая Группа Характеристик', 'admin/'.Route::controller().'/index');
        $this->setModule('filter',array(),'Widgets/Toolbar/Filters/filter_tyr');

        $this->html = Arr::init_object();
        $this->html->title = 'Новая Группа Характеристик';
        $obj = Arr::init_object();

        if ($_POST) {
            $obj = (object)$_POST;
            // Set default settings for some fields
            $obj->name = urldecode($obj->name);
            $obj->alias = Text::translit($obj->name);
            $obj->group = Arr::get( $_POST, 'group','group' );
            $obj->type = Arr::get( $_POST, 'type', 'group');
            $obj->id = rand(1,100000);
            $obj->title = Model::get_TypeTitle($obj->type);
            $ret = DB::select('value')
                ->from('tyr_specifications')
                ->where('name','=',$obj->name)
                ->where('type','=',$obj->type)
                ->execute();
            $obj->set_val = $ret[0]['value'];
        }
        $tyr = Model::get_Tyrs();
        $pole = Model::get_TypePole();
        $group = Model::get_Specif_group();
        $group_specif = Model::get_Group_Specif();
        $this->html->toolbar = View::tpl( array( 'html' => $this->html, ),'Widgets/Toolbar/EditSave');
        $this->html->content = View::tpl( array('module' => $this->_module,'result' => $tyr,
            'pole' => $pole,'group'=>$group,'obj' => $obj, 'specif' => $group_specif),'Tyr/specification/group');
        $this->_content = View::tpl( array( 'html' => $this->html, ),$this->_template );
        //View::tpl( array( 'html' => $this->html, ),$this->_template);
    }

    //Настройки Туров
    public function configAction()
    {
        $this->_seo['h1'] = 'Настройки Туров';
        $this->_seo['title'] = 'Настройки Туров';
        $this->setBreadcrumbs('Настройки Туров', 'admin/'.Route::controller().'/index');
        $this->setModule('filter',array(),'Widgets/Toolbar/Filters/filter_tyr');
        $this->setModule('toolbar',array(),'Widgets/Toolbar/NewDel');

        $this->html = Arr::init_object();
        $this->html->title = 'Настройки Туров';

        $tyr = Model::get_Tyrs();

        $this->html->content = View::tpl( array('module' => $this->_module,'result' => $tyr,),'Tyr/Config');
        $this->_content = View::tpl( array( 'html' => $this->html, ),$this->_template );
        //View::tpl( array( 'html' => $this->html, ),$this->_template);
    }

    public function sellTypeAction(){
        $post = $_POST;
        if ($_POST) {
            $post = $_POST['FORM'];
            // Set default settings for some fields
            $data['name'] = Arr::get( $_POST, 'name', 0 );
            if($data['name'] == ''){
                Msg::factory()->setMsg('Err','icon-warning3','Ошибка!!!','Для начала нужно заполнить поле Название!!!',3000,1);
                $data['msg'] = Msg::factory()->Msgs;
                $data['pole']='field_name';
                $this->send($data);
            }

            $data['type'] = Arr::get( $_POST, 'type', 'input');
            $obj = Arr::init_object();
            $obj->name = $data['name'];
            $obj->type = $data['type'];
            $obj->id = rand(1,100000);
            $obj->title = Model::get_TypeTitle($obj->type);

            $data['control'] = View::tpl(array('obj' => $obj), 'Widgets/Control/Control');

            //Msg::factory()->setMsg('Ok','icon-user-check','Форма обновлена','',2900,0);
            //$data['msg'] = Msg::factory()->Msgs;
            $this->send($data);
        }

    }
    public function send( $data ) {
        echo json_encode( $data );
        Msg::factory()->resetMsg();
        die;
    }
}