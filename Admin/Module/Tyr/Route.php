<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 24.11.2015
 * Time: 18:45
 */
return array(
    'tyr/index' => 'tyr/tyr/index',
    'tyr/add' => 'tyr/tyr/tyrAdd',
    'tyr/addAjax' => 'tyr/TyrAjax/tyr_add',

    'tyr/specifications' => 'tyr/tyr/specif',
    'tyr/specifications/add' => 'tyr/tyr/specif_add',
    'tyr/specifications/edit' => 'tyr/tyr/specif_edit',
    'tyr/specifications/delete' => 'tyr/tyr/specif_delete',
    'tyr/specifications/sellType' => 'tyr/tyr/sellType',

    'tyr/specif/addAjax' => 'tyr/TyrAjax/specif_add',
    'tyr/specif/editAjax' => 'tyr/TyrAjax/specif_edit',

    'tyr/specifications/group' => 'tyr/tyr/specif_group',
    'tyr/specif/groupAjax' => 'tyr/TyrAjax/groupAjax',

    'tyr/config' => 'tyr/tyr/config',

    'manager/getfolder' => 'tyr/FileAjax/getFolder',
    //'articles/page/<page:[0-9]*>' => 'articles/articles/index',
    //'articles/<alias>' => 'articles/articles/inner',
);

