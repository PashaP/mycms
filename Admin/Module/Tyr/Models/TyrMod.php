<?php

/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 25.11.2015
 * Time: 8:24
 */
namespace Admin\Module\Tyr\Models;

use Core\QB\DB;

class TyrMod extends \Admin\Module\Tyr\Controllers\Tyr
{
    public static function get_Tyrs(){
        $tyr = array();
        return $tyr;
    }
    public static function get_Specif_All(){
        $specif = DB::select('tyr_specifications.id','tyr_specifications.name','tyr_specifications.create_at',
            'tyr_specifications.status',
            array(DB::expr('tyr_specif_type.name'), 'type_name'),
            array(DB::expr('tyr_specif_group.name'), 'group_name'))
            ->from('tyr_specifications')
            ->join('tyr_specif_type')->on('tyr_specif_type.alias','=','tyr_specifications.type')
            ->join('tyr_specif_group')->on('tyr_specif_group.alias','=','tyr_specifications.group')
            ->order_by('tyr_specifications.alias','ASC')
            ->as_object()
            ->execute();
        return $specif;
    }
    public static function get_Specif($id){
        $specif = DB::select('*')
            ->from('tyr_specifications')
            ->where('id','=',$id)
            ->as_object()
            ->execute();
        return $specif[0];
    }
    public static function get_TypePole(){
        $_types =  DB::select()
            ->from('tyr_specif_type')
            ->order_by('alias','ASC')
            ->execute();

        return $_types;
    }
    public static function get_Specif_group(){
        $_group =  DB::select()
            ->from('tyr_specif_group')
            ->order_by('alias','ASC')
            ->as_object()
            ->execute();

        return $_group;
    }
    public static function get_Group_Specif(){
        $_group =  DB::select()
            ->from('tyr_specifications')
            ->where('group','=','base')
            ->order_by('alias','ASC')
            ->as_object()
            ->execute();
        return $_group;
    }
    public static function get_TypeTitle($alias){
        $_types =  DB::select('name')
            ->from('tyr_specif_type')
            ->where('alias','=',$alias)
            ->execute();
        //var_dump($_types[0]);
        return $_types[0]["name"];
    }
    // Tyr
    public static function getGroupAll(){
        $ret = DB::select()
                    ->from('tyr_specifications')
                    ->where('group', '=','group')
                    ->where('status','=', 1)
                    ->as_object()
                    ->execute();
        if(count($ret)>0){

            foreach($ret as $obj){
                $tmp = $obj;
                $tmp->sell = explode(',', $tmp->value);
                $tmp->sell_vall = json_decode($tmp->value_group);
                $group[] = $tmp;
            }
            return $group;
        }else{
            return false;
        }
    }
}