<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 27.10.2015
 * Time: 19:51
 */

namespace Admin\Module;

use Core\Config;
//use Core\HTTP;
use Core\HTML;
use Core\QB\DB;
use Core\Route;
use Core\User;
use Core\View;
use Core\Widget;

class Base
{
    protected $_template = 'Main';
    protected $html;
    protected $_content;
    protected $_config = array();
    protected $_seo = array();
    protected $_breadcrumbs = array();
    protected $_method;
    protected $_menu = array();
    protected $_cont;
    protected $_module = array();
    protected $post;

    static $_instance; // Constant that consists self class

    // Instance method
    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }

    public function before() {
        $this->_method = $_SERVER['REQUEST_METHOD'];
        $this->config();

        //$this->redirects();
        //User::factory()->is_remember();
        //$cron = new Cron;
       // $cron->check();
    }

    public function after() {
        //$this->access();
        //$this->seo();
        //$this->visitors();

        $this->setMenu();
        $this->render();
    }
    private function config() {
        $result = DB::select('configure.name', 'configure.value', 'configure.group')
            ->from('configure')
            ->join('configure_groups')->on('configure.group', '=', 'configure_groups.alias')
            ->where('configure.status', '=', 1)
            ->where('configure_groups.status', '=', 1)
            ->find_all();
        $groups = array();
        foreach($result AS $obj) {
            $groups[$obj->group][$obj->name] = $obj->value;
        }
        foreach($groups AS $key => $value) {
            Config::set($key, $value);
        }
        //$result = DB::select('script', 'place')->from('seo_scripts')->where('status', '=', 1)->as_object()->execute();
        //$this->_seo['scripts'] = array('body' => array(), 'head' => array(), 'counter' => array());
        //foreach ( $result AS $obj ) {
        //    $this->_seo['scripts'][ $obj->place ][] = $obj->script;
        //}

        $this->setBreadcrumbs('Главная', '');
    }

    private function render() {
        if( Config::get( 'error' ) ) {
            $this->_template = '404';
        }
        $this->_breadcrumbs = HTML::backendBreadcrumbs($this->_breadcrumbs);
        $data = array();
        foreach ($this as $key => $value) {
            $data[$key] = $value;
        }
        //$data['GLOBAL_MESSAGE'] = System::global_massage();
        echo HTML::compress(View::tpl($data, $this->_template));
    }

    protected function setBreadcrumbs( $name, $link = NULL ) {
        $this->_breadcrumbs[] = array('name' => $name, 'link' => $link);
    }
    protected function setMenu(){
        $_menu = Widget::factory()->getMenuAdmin();
        $this->menu = View::tpl(array('menu' => $_menu,),'Widgets/Sidebar');
    }
    protected function setModule($name,$param,$url){
        $this->_module[$name] = View::tpl($param,$url);
    }

}