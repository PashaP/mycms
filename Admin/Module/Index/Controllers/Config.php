<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 21.11.2015
 * Time: 15:03
 */

namespace Admin\Module\Index\Controllers;

use Core\Arr;
use Core\QB\DB;
use Core\Route;
use Core\View;
use Core\Common;
use Core\HTTP;
use Core\Msg;
use Core\Widget;

class Config  extends \Admin\Module\Base
{
    public function before()
    {
        //$this->setMenu();
        $this->setBreadcrumbs('Главная', 'admin/');
        $this->_template = 'Main';
        $this->html = Arr::init_object();
    }

    public function indexAction()
    {
        $this->_seo['h1'] = 'Настройки Сайта';
        $this->_seo['title'] = 'Настройки Сайта';
        $this->setBreadcrumbs('Настройки', 'admin/'.Route::controller().'/index');
        $this->html = Arr::init_object();
        $this->html->title = 'Главная';

        //=================
        if ($_POST) {
            $result = Common::factory('configure')->getRows(1);
            $errors = array();

            foreach($result AS $obj) {
                if (array_key_exists($obj->group.'-'.$obj->name, $_POST)) {
                    $value = Arr::get($_POST, $obj->group.'-'.$obj->name);
                    if( $value === NULL && $obj->valid ) {
                        $errors[] = 'Параметр "'.$obj->name.'" должен быть заполнен!';
                    }
                } else if($obj->type != 'checkbox') {
                    $errors[] = 'Параметр "'.$obj->name.'" должен быть заполнен!';
                }
            }

            if( !$errors ) {
                foreach($result AS $obj) {
                    if (array_key_exists($obj->group.'-'.$obj->name, $_POST)) {
                        $value = Arr::get($_POST, $obj->group.'-'.$obj->name);
                        DB::update('configure')->set(array(
                            'value' => $value
                        ))->where('name', '=', $obj->name)->where('group', '=', $obj->group)->execute();
                    } else if($obj->type == 'checkbox') {
                        DB::update('configure')->set(array(
                            'value' => 0
                        ))->where('name', '=', $obj->name)->where('group', '=', $obj->group)->execute();
                    }
                }
                $this->html->msg = Msg::factory()->setMessages('ok','Сохранение', 'Вы успешно изменили данные!');
                //$this->html->msg = View::tpl( array( 'result' => $res, 'groups' => $grop,),'Widgets/HTML/Msg');
                //Msg::GetMessage(1, 'Вы успешно изменили данные!');
                //HTTP::redirect( 'admin/'.Route::controller().'' );
            }else{
                $this->html->msg = Msg::factory()->setMessages('err','Ошибка', 'Вы где то допустили ошибку!');
            }
        }
        //===================================
        $result = Common::factory('configure')->getRows(1, 'sort', 'ASC');

        $res = array();
        foreach($result AS $obj) {
            $res[$obj->group][] = $obj;
        }

        $_groups = Common::factory('configure_groups')->getRows(1, 'sort', 'ASC');
        $grop = array();
        foreach($_groups AS $group) {
            $grop[$group->side][] = $group;
        }

        $this->html->toolbar = View::tpl( array( 'html' => $this->html, ),'Widgets/Toolbar/EditSaveOnly');
        $this->html->content = View::tpl( array( 'result' => $res, 'groups' => $grop,),'Index/Config');
        $this->_content = View::tpl( array(), $this->_template );

    }
}