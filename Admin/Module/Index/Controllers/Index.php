<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 03.11.2015
 * Time: 23:11
 */

namespace Admin\Module\Index\Controllers;

use Core\Arr;
use Core\Route;
use Core\View;
use Admin\Module\User\Models\User as mUser;

class Index extends \Admin\Module\Base
{
    public function before()
    {
        $this->setBreadcrumbs('Главная', 'admin/');
        $this->_template = 'Main';
        $this->html = Arr::init_object();
    }

    public function indexAction()
    {
        $this->_seo['h1'] = 'Панель управления';
        $this->_seo['title'] = 'Панель управления';
        $this->setBreadcrumbs('Панель управления', 'admin/'.Route::controller().'/index');

        $this->html = Arr::init_object();
        $this->html->title = 'Главная';

        $this->html->count->module = Route::factory()->count_Module();
        $this->html->count->users = mUser::count_User();
        $this->html->count->orders = 0;
        $this->html->count->comments = 0;
        $this->html->count->catalog = 0;

        $this->html->content = View::tpl( array('count'=>$this->html->count, ),'Index/Main');
        $this->_content = View::tpl( array( 'html' => $this->html, ),$this->_template );
        //View::tpl( array( 'html' => $this->html, ),$this->_template);

    }
    public function moduleAction(){
        $this->html->title = 'Модули';
        $this->_seo['h1'] = 'Панель управления Модулями';
        $this->_seo['title'] = 'Панель управления Модулями';
        $this->setBreadcrumbs('Модули', 'admin/'.Route::controller().'/index');

        $this->result = Route::factory()->get_Module();

        $this->html->content = View::tpl( array('result' => $this->result, 'table' => 'module', ),'Index/Index');
        $this->_content = View::tpl( array(),$this->_template );
    }
    public function statisticAction(){
        $this->html->title = 'Модули';
        $this->_seo['h1'] = 'Статистика по сайту';
        $this->_seo['title'] = 'Статистика по сайту';
        $this->setBreadcrumbs('Статистика', 'admin/'.Route::controller().'/index');

        $this->result = Route::factory()->get_Module();

        $this->html->content = View::tpl( array('result' => $this->result, 'table' => 'module', ),'Index/Statistic');
        $this->_content = View::tpl( array(),$this->_template );
    }
}