<?php

/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 11.11.2015
 * Time: 14:08
 */
namespace Admin\Module\Ajax\Controllers;

use Core\Core;
use Core\QB\DB;
use Core\Arr;
use Core\User;
use Core\Config AS conf;
use Core\View;
use Core\Msg;
//use Core\System;
//use Core\Log;
use Core\Email;
use Module\Base;

//use Core\Message; Admin\Module\Ajax
//use Core\Common;
class Ajax  extends \Admin\Module\Ajax
{
    protected $post;
    protected $get;
    protected $files;
    protected $Err;

    function before() {
        parent::before();

        $this->post = $_POST;
        $this->get = $_GET;
        $this->files = $_FILES;

        if (!$this->post) {
            Msg::factory()->setMsg('Err','icon-warning3','Ошибка Авторизации!!!','Wrong request!',3000,1);
            $this->error();
        }
    }
    public function indexAction()
    {
        Core::factory()->GetMessage(1, 'Вы успешно авторизовались на сайте!', 3500);
        Msg::factory()->setMsg('Ok','icon-warning3','Успешная Авторизации!!!','Вы успешно авторизовались на сайте!',3000,1);
        $this->success();
    }
    // User authorization
    public function loginAction() {
        $login = Arr::get( $this->post, 'login' );
        $password = Arr::get( $this->post, 'password' );
        $remember = Arr::get( $this->post, 'remember' );
        if (!$login) {
            Msg::factory()->setMsg('Err','icon-warning3','Ошибка Авторизации!!!','Вы не ввели логин',3000,1);
            $this->error();
        }
        if (!$password) {
            Msg::factory()->setMsg('Err','icon-warning3','Ошибка Авторизации!!!','Вы не ввели пароль!',3000,2);
            $this->error();
        }
        // Check user for existance and ban
        $user = User::factory()->getUser($login,$password);
        //var_dump($user);
        if( !$user ) {
            Msg::factory()->setMsg('Err','icon-warning3','Ошибка Авторизации!!!','Вы допустили ошибку в логине и/или пароле!',3000,3);
            $this->error();
        }

        if(User::factory()->Users->blocked > 0 ) {
            Msg::factory()->setMsg('Err','icon-remove-user','Ошибка Авторизации!!!','Пользователь с указанным Логином либо заблокирован либо не активирован. Пожалуйста обратитесь к Администратору для решения сложившейся ситуации',10000,0);
            $this->error();
        }

        //$this->error(array('title' => 'Ошибка Авторизации!!!','text' => 'Вы допустили ошибку в логине и/или пароле!' ));
         //Authorization of the user
       // DB::update('users')->set(array('last_login' => time(), 'logins' => (int) $user->logins + 1, 'updated_at' => time()))->where('id', '=', $user->id)->execute();
        //User::factory()->auth( $user, $remember );
        //echo '2222';
        Core::factory()->GetMessage(1, 'Вы успешно авторизовались на сайте!', 3500);
        Msg::factory()->setMsg('Ok','icon-user-check','Успешная Авторизация!!!','',2900,0);
        $this->success();
    }
    // set param Status
    public function setStatusAction() {
        $table = $this->post['table'];
        $pole = $this->post['pole'];
        $id = $this->post['id'];
        $val = DB::select($pole)//'active','status'
                ->from($table)
                ->where('id','=',$id)
                ->execute();
        if($pole == "active"  and isset($val[0]["active"])){
            $val = !(bool)$val[0]["active"];
            $data = array('active' => $val,);
        }
        if($pole == "status"  and isset($val[0]["status"])){
            $val = !(bool)$val[0]["status"];
            $data = array('status' => $val,);
        }

         $ret = DB::update($table)->set($data)->where('id', '=', $id)->execute();
        $this->answer(array('status' => $val));
    }

}