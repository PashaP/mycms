<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 21.11.2015
 * Time: 8:54
 */

namespace Admin\Module;

use Core\Msg;

class Ajax extends Base
{
    protected $post;
    protected $get;
    protected $files;
    protected $Err;

    public function before() {
        $this->post = $_POST;
        $this->get = $_GET;
        $this->files = $_FILES;
    }
    public static function Aj_Answer($data){
        echo json_encode( $data );
        Msg::factory()->resetMsg();
        die;
    }
    public static function Aj_Success($redir = '') {
        $data['msg'] = Msg::factory()->Msgs;
        if($redir != '')
            $data['redirect'] = $redir;
        if( !is_array( $data ) ) {
            $data = array(
                'response' => $data,
                'redirect' => $redir,
                'noclear' => 1,
            );
        }
        $data['success'] = true;
        echo json_encode( $data );
        Msg::factory()->resetMsg();
        die;
    }
    public static function Aj_Error() {
        $data = Msg::factory()->Msgs;
        if( !is_array( $data ) ) {
            $data = array(
                'response' => $data,
            );
        }
        $data['success'] = false;
        echo json_encode( $data );
        Msg::factory()->resetMsg();
        die;
    }
    // Generate Ajax answer
    protected function answer( $data ) {
        echo json_encode( $data );
        Msg::factory()->resetMsg();
        die;
    }


    // Generate Ajax success answer
    protected function success() {
        $data = Msg::factory()->Msgs;
        if( !is_array( $data ) ) {
            $data = array(
                'response' => $data,
                'redirect' => 'admin/index',
                'noclear' => 1,
            );
        }
        $data['success'] = true;
        $this->answer( $data );
    }


    // Generate Ajax answer with error
    protected function error() {
        $data = Msg::factory()->Msgs;

        if( !is_array( $data ) ) {
            $data = array(
                'response' => $data,
            );
        }
        $data['success'] = false;
        $this->answer( $data );
    }

}