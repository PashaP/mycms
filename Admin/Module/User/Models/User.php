<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 19.11.2015
 * Time: 7:33
 */

namespace Admin\Module\User\Models;

use Core\QB\DB;

class User
{
    public static function count_User(){
        $result = DB::select(array(DB::expr('COUNT(id)'), 'count'))
            ->from('user')
            ->where('blocked', '=', 0);
        return $result->count_all();
    }
}