//var generate = function( message, type ) {
//    $(document).alert2({
//        message: message,
//        headerCOntent: false,
//        footerContent: false,
//        typeMessage: type //false or 'warning','success','info','danger'
//    });
//    setTimeout(function(){
//        $.magnificPopup.close();
//    }, 2500);
//}

var closePopup = function( it ) {
    it.fadeOut(650);
    setTimeout(function(){
        it.remove();
    }, 700);
};
var generate = function( message, type, time ) {
    var mainBlock = $('#fPopUp');
    var current;
    if(!mainBlock.length) {
        $('<div id="fPopUp"></div>').appendTo('body');
        mainBlock = $('#fPopUp');
    }
    var i = 1;
    var count = 0;
    mainBlock.find('.content').each(function(){
        current = parseInt($(this).data('i'));
        if(current + 1 > i) {
            i = current + 1;
        }
        count++;
    });
    if(count >= 5) {
        mainBlock.find('div.content:first-child').remove();
    }
    $('<div class="content ' + type + '" data-i="' + i + '" style="display: none;">' + message + '</div>').appendTo(mainBlock);
    mainBlock.find('div.content[data-i="' + i + '"]').fadeIn(200);
    if(time) {
        setTimeout(function(){
            closePopup(mainBlock.find('div.content[data-i="' + i + '"]'));
        }, time);
    }
};

var loader = function( selector, type ) {
    if( type == 0 ) {
        $(selector).removeClass('refRel').removeClass('refShow');
    }
    if( type == 1 ) {
        var refPos = $(selector).css('position');
        if(refPos == 'static'){
            $(selector).addClass('refRel');
        }
        $(selector).addClass('refShow');
    }
};

$(document).ready(function(){
    $('body').on('click', '#fPopUp div.content', function(){ closePopup($(this)); });

    var pickerInit = function( selector ) {
        if(!$(selector).length) {
            return false;
        }
        $(selector).each(function(){
            var date = $(this).val();
            $(this).datepicker({
                showOtherMonths: true,
                selectOtherMonths: false
            });
            $(this).datepicker('option', $.datepicker.regional['ru']);
            var dateFormat = $(this).datepicker( "option", "dateFormat" );
            $(this).datepicker( "option", "dateFormat", 'dd.mm.yy' );
            $(this).val(date);
        });
    };
    pickerInit('.fPicker');

    $('.clearMinifyCache').on('click', function (e) {
        e.preventDefault();
        if(confirm('Вы точно хотите удалить минифицированные файлы?')) {
            var it = $(this);
            $.ajax({
                url:'/admin/ajax/clearMinifyCache',
                success: function(data) {
                    it.closest('li').remove();
                }
            });
        }
    });


    $('body').on('click', '.translitAction', function(){
        var it = $(this);
        var trans = it.data('trans');
        var source;
        if(trans) {
            source = $('.translitSource[data-trans="' + trans + '"]').val();
        } else {
            source = $('.translitSource').val();
        }
        $.ajax({
            url: '/admin/ajax/translit',
            type: 'POST',
            dataType: 'JSON',
            data: {
                source: source
            },
            success: function(data) {
                if(!it.data('trans')) {
                    $('.translitConteiner').val(data.result);
                } else {
                    $('.translitConteiner[data-trans="' + it.data('trans') + '"]').val(data.result);
                }
            }
        });
    });

    var change_status = function( it, id ) {
        var current = it.data('status');
        var table = $('#parameters').data('table');
        var pole = $('#parameters').data('pole');
        $.ajax({
            url: '/admin/ajax/ajax/setStatus',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id,
                current: current,
                table: table,
                pole: pole
            },
            success: function(data){
                it.data('status', data.status);
                var html;
                if(data.status == 1) {
                    html = '<i class="fa fa-check-square-o"></i>';
                    it.removeClass('btn-danger');
                    it.addClass('btn-success');
                } else {
                    html = '<i class="fa fa-dot-circle-o"></i>';
                    it.removeClass('btn-success');
                    it.addClass('btn-danger');
                }
                it.html(html);
                $('.liTipContent').remove();
                $('.bs-tooltip').each(function(){
                    $(this).liTip();
                });
                it.trigger('mouseenter');
            }
        });
    };
    $('.setStatus').on('click', function(e){
        e.preventDefault();
        var it = $(this);
        var id;
        if( it.attr( 'data-id' ) ) {
            id = it.attr( 'data-id' );
        } else {
            id = it.closest('li').data('id');
        }
        change_status( it, id );
    });

    $('.toolbar').on('click', '.delete-items', function(e){
        e.preventDefault();
        var ids = [];
        var id;
        $('input[type="checkbox"]').each(function(){
            if( $(this).prop('checked') ) {
                id = $(this).closest('li').data('id');
                if( id ) {
                    ids.push( id );
                } else {
                    ids.push( $(this).closest('tr').data('id') );
                }
            }
        });
        if( ids.length ) {
            if( !confirm( 'Это действие необратимо. Продолжить?' ) ) {
                return false;
            }
            loader( '.contentWrapMar', 1 );
            var table = $('#parameters').data('table');
            $.ajax({
                url: '/admin/ajax/deleteMass',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    ids: ids,
                    table: table
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        } else {
            generate( 'Нечего удалять!', 'warning' );
        }
    });

    $('.toolbar').on('click', '.publish', function(e){
        e.preventDefault();
        var ids = [];
        $('input[type="checkbox"]').each(function(){
            if( $(this).prop('checked') ) {
                id = $(this).closest('li').data('id');
                if( id ) {
                    ids.push( id );
                } else {
                    ids.push( $(this).closest('tr').data('id') );
                }
            }
        });
        if( ids.length ) {
            loader( '.contentWrapMar', 1 );
            var table = $('#parameters').data('table');
            var status = $(this).data('status');
            $.ajax({
                url: '/admin/ajax/setStatusMass',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    status: status,
                    ids: ids,
                    table: table
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        } else {
            generate( 'Нечему задавать статус!', 'warning' );
        }
    });

    if($('.changeField').length) {
        $('.changeField').on('click', function(e){
            e.preventDefault();
            var it = $(this);
            var id = it.data('id');
            var field = it.data('field');
            var table = $('#parameters').data('table');
            $.ajax({
                url: '/admin/ajax/change_field',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    id: id,
                    field: field,
                    table: table
                },
                success: function(data){
                    if(data.current) {
                        it.text('Да');
                    } else {
                        it.text('Нет');
                    }
                }
            });
        });
    }

    if ( $('#orderParameters').length ) {
        $('button').on('click', function(){
            if( $(this).attr('href') ) {
                window.location.href = $(this).attr('href');
            } else {
                var it = $(this);
                var form = it.closest('.widgetContent');
                var action = form.data('ajax');
                if( action ) {
                    $.ajax({
                        url: '/admin/ajax/' + action,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            data: form.find('input,select,textarea').serializeArray(),
                            id: $('#orderParameters').data('id')
                        },
                        success: function(data) {
                            if(data.success) {
                                if(data.msg) {
                                    generate(data.msg, 'success');
                                }
                            } else {
                                if(data.msg) {
                                    generate(data.msg, 'warning');
                                }
                            }
                        }
                    });
                }
            }
        });
        $('select[name="delivery"]').on('change', function() {
            if( $(this).val() == 2 ) {
                $('input[name="number"]').closest('.form-group').show();
            } else {
                $('input[name="number"]').closest('.form-group').hide();
            }
        });

        var setAmount = function(){
            var amount = 0;
            var pos = 0;
            $('#orderItemsList tr').each(function(){
                var cost = parseInt( $(this).find('.tableOrderItemsCost').text() );
                var count = parseInt( $(this).find('.count').val() );
                amount += cost * count;
                pos += 1;
            });
            $('#orderAmount span').text(amount);
            $('#orderPositionsCount').text(pos);
        };
        $('#orderItems').on('click', function(){
            var it = $(this);
            var table = it.closest('.widgetContent').find('table');
            table.find('tr').each(function(){
                var it = $(this);
                var count = it.find('.count').val();
                var catalog_id = it.find('.catalog_id').val();
                var size_id = it.find('.size_id').val();
                $.ajax({
                    url: '/admin/ajax/orders/orderItems',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        count: count,
                        catalog_id: catalog_id,
                        size_id: size_id,
                        id: $('#orderParameters').data('id')
                    },
                    success: function(data) {}
                });
            });
            setAmount();
        });
        $('.orderPositionDelete').on('click', function(e){
            e.preventDefault();
            var it = $(this).closest('tr');
            var count = it.find('.count').val();
            var catalog_id = it.find('.catalog_id').val();
            var size_id = it.find('.size_id').val();
            $.ajax({
                url: '/admin/ajax/orders/orderPositionDelete',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    count: count,
                    catalog_id: catalog_id,
                    size_id: size_id,
                    id: $('#orderParameters').data('id')
                },
                success: function(data) {
                    if (data.success) {
                        it.remove();
                        setAmount();
                    }
                }
            });
        });
    }
});


//////////////// DAMN UPLOADER
$(function(){
    var dropzone = $('.dropZone');
    if(dropzone.length) {
        var upl = dropzone.data('upload');
        var sort = dropzone.data('sortable');
        var def = dropzone.data('default');

        var getUploadedPhotos = function() {
            $.ajax({
                type: 'POST',
                url: '/admin/ajax/' + upl,
                dataType: 'JSON',
                success: function(data){
                    $('.dropDownload').html(data.images);
                    if( parseInt(data.count) ) {
                        $('.loadedBox .checkAll').fadeIn(300);
                    }
                }
            });
        };
        getUploadedPhotos();

        $('.dropDownload').sortable({
            connectWith: ".loadedBlock",
            handle: ".loadedDrag",
            cancel: '.loadedControl',
            placeholder: "loadedBlockPlaceholder",
            change: function(){
                var order = [];
                $(this).find('.loadedBlock').each(function(){
                    order.push($(this).data('image'));
                });
                $.ajax({
                    type: "POST",
                    url: "/admin/ajax/" + sort,
                    data: {
                        order: order
                    }
                });
            }
        });

        $('.dropDownload').on('click', '.loadedCover .btn.btn-success', function(){
            var it = $(this),
                itP = it.closest('.loadedBlock'),
                id = itP.data('image');
            $.ajax({
                url: '/admin/ajax/' + def,
                type: 'POST',
                data: {
                    id: id
                }
            });
        });
    }
});


//////////////// RELATED ITEMS
$(function(){
    var interval;
    var list = $('#relatedList');
    var limit = parseInt(list.data('limit'));
    var page = 1;
    var pages = 1;
    var search = function() {
        loader(list, 1);
        var data = {};
        var key;
        var value;
        $('#relatedItemsBlock input,#relatedItemsBlock select').each(function () {
            key = $(this).data('name');
            value = $(this).val();
            data[key] = value;
        });
        data['id'] = list.data('item');
        data['limit'] = limit;
        data['page'] = page;
        $.ajax({
            url: '/admin/ajax/catalog/searchItems',
            type: 'POST',
            dataType: 'JSON',
            data: data,
            success: function (resp) {
                if(!parseInt(resp.count)) {
                    list.html('<p class="relatedMessage">По заданным параметрам товары не найдены!</p>');
                    page = 1;
                } else {
                    var pager = '';
                    if(resp.count > limit) {
                        pager += '<div class="relatedPager">';
                        pages = Math.ceil(resp.count / limit);
                        for(var j = 1; j <= pages; j++) {
                            if(j == page) {
                                pager += '<span class="active">' + j + '</span>';
                            } else {
                                pager += '<span>' + j + '</span>';
                            }
                        }
                        pager += '</div>';
                    } else {
                        page = 1;
                    }
                    var html = pager;
                    for (var i = 0; i < resp.items.length; i++) {
                        html += '<div class="relatedItem" data-id="' + resp.items[i].id + '">';
                        if (resp.items[i].image) {
                            html += '<img src="' + resp.items[i].image + '" alt="">';
                        }
                        html += '<div class="relatedName">' + resp.items[i].name + '<br>' + resp.items[i].cost + ' грн</div>';
                        html += '</div>';
                    }
                    html += pager;
                    list.html(html);
                }
                loader(list, 0);
            }
        });
    };
    var listBlock = $('#relatedItemsBlock');
    listBlock.on('change', 'select', function(){
        search();
    });
    listBlock.on('keypress', 'input', function(){
        clearTimeout(interval);
        interval = setTimeout(function(){
            search();
        }, 500);
    });

    list.on('click', '.relatedItem', function(){
        loader(list, 1);
        var it = $(this);
        var clone = it.clone();
        $.ajax({
            dataType: "json",
            type : 'POST',
            data : {
                who_id: $('#relatedList').data('item'),
                with_id: $(this).data('id')
            },
            url: '/admin/ajax/catalog/addItemToRelated',
            success: function(data) {
                if(data.success) {
                    clone.addClass('active');
                    clone.appendTo('.listSimilar');
                    search();
                } else {
                    generate(data.msg, 'warning');
                }
            }
        });
    });

    $('.listSimilar').on('click', '.relatedItem', function(){
        if(!confirm('Удалить товар из сопутствующих?')) {
            return false;
        }
        var it = $(this);
        var with_id = it.data('id');
        var who_id = $('#relatedList').data('item');
        $.ajax({
            dataType: "json",
            type : 'POST',
            data : {
                who_id: who_id,
                with_id: with_id
            },
            url: '/admin/ajax/catalog/removeItemFromRelated',
            success: function(data) {
                if(data.success) {
                    it.remove();
                    search();
                }
            }
        });
    });

    list.on('click', '.relatedPager span', function() {
        var it = $(this);
        if(it.hasClass('active')) { return false; }
        page = parseInt(it.text());
        list.find('.relatedPager span.active').removeClass('active');
        list.find('.relatedPager span:nth-child(' + page + ')').addClass('active');
        search();
    });
});


/////////// ADD POSITION TO ORDER
$(function(){
    var listBlock = $('#orderItemsBlock');
    if(listBlock.length) {
        var interval;
        var list = $('#orderItemsList');
        var limit = parseInt(list.data('limit'));
        var page = 1;
        var pages = 1;
        var active = 0;
        var search = function() {
            loader(list, 1);
            var data = {};
            var key;
            var value;
            $('#orderItemsBlock input,#orderItemsBlock select').each(function () {
                key = $(this).data('name');
                value = $(this).val();
                data[key] = value;
            });
            data['id'] = list.data('item');
            data['limit'] = limit;
            data['page'] = page;
            $.ajax({
                url: '/admin/ajax/catalog/searchItems',
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function (resp) {
                    if(!parseInt(resp.count)) {
                        list.html('<p class="relatedMessage">По заданным параметрам товары не найдены!</p>');
                        page = 1;
                    } else {
                        var pager = '';
                        if(resp.count > limit) {
                            pager += '<div class="relatedPager">';
                            pages = Math.ceil(parseInt(resp.count) / limit);
                            for(var j = 1; j <= pages; j++) {
                                if(j == page) {
                                    pager += '<span class="active">' + j + '</span>';
                                } else {
                                    pager += '<span>' + j + '</span>';
                                }
                            }
                            pager += '</div>';
                        } else {
                            page = 1;
                        }
                        var html = pager;
                        for (var i = 0; i < resp.items.length; i++) {
                            if(active == resp.items[i].id) {
                                html += '<div class="relatedItem active" data-id="' + resp.items[i].id + '">';
                            } else {
                                html += '<div class="relatedItem" data-id="' + resp.items[i].id + '">';
                            }
                            if (resp.items[i].image) {
                                html += '<img src="' + resp.items[i].image + '" alt="">';
                            }
                            html += '<div class="relatedName">' + resp.items[i].name + '<br>' + resp.items[i].cost + ' грн</div>';
                            html += '</div>';
                        }
                        html += pager;
                        list.html(html);
                    }
                    loader(list, 0);
                }
            });
        };
        listBlock.on('change', 'select', function(){
            search();
        });
        listBlock.on('keypress', 'input', function(){
            clearTimeout(interval);
            interval = setTimeout(function(){
                search();
            }, 500);
        });
        list.on('click', '.relatedPager span', function() {
            var it = $(this);
            if(it.hasClass('active')) { return false; }
            page = parseInt(it.text());
            list.find('.relatedPager span.active').removeClass('active');
            list.find('.relatedPager span:nth-child(' + page + ')').addClass('active');
            search();
        });
        var itemPlace = $('#itemPlace');
        list.on('click', '.relatedItem', function(){
            loader(list, 1);
            list.find('.relatedItem.active').removeClass('active');
            var it = $(this);
            active = it.data('id');
            it.addClass('active');
            $('#orderItemId').val(active);
            $.ajax({
                url: '/admin/ajax/catalog/getItem',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    id: active
                },
                success: function(resp){
                    if(resp.success) {
                        var html = '';
                        if (resp.item.image) {
                            html += '<a href="' + resp.item.image_big + '" rel="lightbox">';
                            html += '<img src="' + resp.item.image + '" class="someImage" />';
                            html += '</a>';
                        }
                        html += '<div class="someBlock"><a href="/admin/items/edit/' + resp.item.id + '" target="_blank">' + resp.item.name + '</a></div>';
                        if(resp.item.brand_id && resp.item.brand_name) {
                            html += '<div class="someBlock"><b>Бренд:</b> <a href="/admin/brands/edit/' + resp.item.brand_id + '" target="_blank">' + resp.item.brand_name + '</a></div>';
                        }
                        html += '<div class="someBlock"><b>Цена:</b> ' + resp.item.cost + ' грн</div>';
                        itemPlace.html(html);
                    }
                    loader(list, 0);
                }
            });
        });
    }
});